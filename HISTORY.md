# History

## 1.0.0

- renamed public functions/classes (made functions uniformly camelCase):
  - ifdo.py
    - iFDOFromFile -> ifdoFromFile
    - iFDO_Reader -> IfdoReader
    - iFDO.set_ifdo_header_fields -> iFDO.setHeaderFields
    - iFDO.update_ifdo_header_fields -> iFDO.updateHeaderFields
    - iFDO.set_header_image_project_uri_to_osis_expedition_url -> iFDO.trySetHeaderImageProjectUriToOsisExpeditionUrl
    - iFDO.set_header_image_event_uri_to_osis_event_url -> iFDO.trySetHeaderImageEventUriToOsisEventUrl
    - iFDO.set_header_equipment_uri_to_handle_url -> iFDO.trySetHeaderEquipmentUriToHandleUrl
    - iFDO.set_image_set_name_field_from_project_event_sensor -> iFDO.setImageSetNameFieldFromProjectEventSensor
    - iFDO.write_ifdo_file -> iFDO.writeIfdoFile
    - iFDO.get_ifdo_file_name -> iFDO.getIfdoFileName
    - iFDO.set_header_fields -> iFDO.setHeaderFields
    - iFDO.update_header_fields -> iFDO.updateHeaderFields
    - iFDO.create_fields -> iFDO.createFields
    - iFDO.update_fields -> iFDO.updateFields
    - iFDO.create_start_time_file -> iFDO.createStartTimeFile
    - iFDO.create_uuid_file -> iFDO.createUuidFile
    - iFDO.create_image_sha256_file -> iFDO.createImageSha256File
    - iFDO.create_image_navigation_file -> iFDO.createImageNavigationFile
    - iFDO.set_image_set_attitude -> iFDO.setImageSetAttitude
    - iFDO.create_image_attitude_file -> iFDO.createImageAttitudeFile
    - iFDO.add_item_info_tab_file -> iFDO.addItemInfoTabFile
    - iFDO.create_acquisition_settings_exif_file -> iFDO.createAcquisitionSettingsExifFile
    - iFDO.remove_item_info_tab_file -> iFDO.removeItemInfoTabFile
    - iFDO.try_set_license_uri_from_license_name -> iFDO.trySetLicenseUriFromLicenseName
    - iFDO.get_header_image_project_uri_to_osis_expedition_url -> iFDO.getHeaderImageProjectUriToOsisExpeditionUrl
    - iFDO.get_header_image_event_uri_to_osis_event_url -> iFDO.getHeaderImageEventUriToOsisEventUrl
    - iFDO.get_equipment_handle_url -> iFDO.getEquipmentHandleUrl
  - osis.py
    - get_expedition_id_from_label -> getExpeditionIdFromLabel
    - get_expedition_url -> getExpeditionUrl
    - get_expedition_id_from_url -> getExpeditionIdFromUrl
    - get_osis_event_url -> getOsisEventUrl
    - get_expedition_event_ids -> getExpeditionEventIds
    - get_expedition_ids -> getExpeditionIds
    - get_expeditions_from_label -> getExpeditionsFromLabel
    - get_event_url -> getEventUrl
  - core.py
    - get_ifdo_fields -> getIfdoFields
    - is_valid_uuid -> isValidUuid
    - resolve_jsonschema -> resolveJsonschema
    - jsonschmea_type_2_python_type -> jsonschemaType2PythonType
  - tests.py
    - validate_against_schema -> validateAgainstSchema
    - are_valid_ifdo_fields -> areValidIfdoFields
    - validate_ifdo -> validateIfdo
    - validate_ifdo_datetimes -> validateIfdoDatetimes
    - validate_ifdo_file_names -> validateIfdoFileNames
    - is_valid_datetime_str -> isValidDatetimeStr
    - check_datetime_str_format -> checkDatetimeStrFormat
    - is_valid_ifdo_field -> isValidIfdoField
    - make_additinal_checks_not_covered_by_schema -> makeAdditinalChecksNotCoveredBySchema

- added writing uuid to mkv file

## 0.7.0

- fixed bug that a photo's item value was created as an array with a singe object instead of just an object
- loaded iFDO files still containing this bug are updated automatically

## 0.6.13

- fix bug in osis api handling

## 0.6.12

- added logging fo ingore subfolders

## 0.6.11

  - added option to ignore subfolders while iFDO creation
  - fixed osis api calls
  - added more descriptive logging/printing in create_image_sha256_file

## 0.6.10

  - cleaned up the print outputs a bit

## 0.6.9

  - auto setting equipment uri now returns handle url instead of equipment git url

## 0.6.8

  - added convenience functions (get_license_uri(), set_license_uri_from_license_name()) to get/set license uri from name

## 0.6.7

  - photo uuids are now explicitly looked for in exif tag 0xa420 ImageUniqueID. Before also the canon tag 0x0028 ImageUniqueID was accepted which, however, is currently not detected by exiv2 as used on the Elements system. 

## 0.6.6

  - renamed geo.addLeverarms2Latlon to geo.addLeverarms2LatLonDepAlt() which now also includes correction of altitude

## 0.6.5

  - fix for update in osis api

## 0.6.4

  - added sorting of entries in intermediate hash and uuid files

## 0.6.3

  - fixed bug lat/lon median windowing in navigation.smoothPositionsSliding2dMedianLatLonGaussianDepth()

## 0.6.2

  - removed package ```referencing``` from setup install_requires as it's not compatible with e.g. pdflatex (in export-tool) and is only needed for jsonschema>=4.18

## 0.6.1

  - fixed requirements

## 0.6.0

  - adapted to iFDOv2.0.1
  - added check if ```jsonschema[format]``` is installed

## 0.5.20

  - replaced to be deprecated jsonschema.RefResolver

## 0.5.19

  - added handling of osis api requests if offline

## 0.5.18

  - fixed bug in provenance logging leading to unremoved tmp file

## 0.5.17

  - added stripping if all string entries

## 0.5.16

  - added option 'as_zip' in iFDO.write_ifdo_file() to write file as zip and added capability to open ifdo zip file

## 0.5.15

  - added check if file &lt;file&gt;_original already exists in image.writeUUIDtoVideo and image.writeUUIDtoPhoto

## 0.5.14

  - iFDO: changed third component in geojson coordinate from positive depth to negative depth values to match 'elevation'

## 0.5.13

  - iFDO: fixed bug in progress indicator

## 0.5.12

  - iFDO: added get functions for OSIS and equipment urls


## 0.5.11

  - added iFDO.set_header_image_project_uri_to_osis_expedition_url()
  - added iFDO.set_header_equipment_uri_to_equipment_git_url()

## 0.5.10

  - added iFDO.set_header_image_event_uri_to_osis_event_url()
  - added source.osis with osis api call to get osis' expedition and event ids

## 0.5.9

  - removed unnecessary and slow items update in iFDO.createiFDO() for headerOnly case 

## 0.5.8

  - renamed 'image-local-path' to 'image-set-local-path' according to iFDO-v2.0.0
  - fixed bug in ifdo.extractConstDataValues() for case of only one item
  - updated iFDO_Reader to use the faster json loader

## 0.5.7

  - fixed navigation.removeDuplicateAttitudes() for case of starting with None values
  - fixed naviagtion.removeDuplicatePositions() for case of starting with None values


## 0.5.6

  - fixed bug with None data in navigation.removeDuplicateAttitudes()
  - fixed bug in ifdo.writeParsedDataToItems() with None data in extractCons
  - unknown values, i.e. 0.0, are now ignored in iFDO._set_rep_header_fields_dt_lat_lon_alt()

## 0.5.5

  - made createiFDO() by default not check individual items for sake of speed
  - iFDO._set_rep_header_fields_dt_lat_lon_alt(): added image-coordinate-uncertainty
  - iFDO._set_rep_header_fields_dt_lat_lon_alt(): values except for image-datetime are not extracted as the all itmes median
  - fixed inversion bug in iFDO.create_image_navigation/attitude_file()

## 0.5.4

  - fixed variable naming typo

## 0.5.3

  - made iFDO option 'ignore_image_files' more consequent and useful for pure header updates

## 0.5.2

  - fixed wrong ifdo file renamed warning showing even if no ifdo file existed in the first place
  - fixed wrong warning that ifdo file name would not contain sensor name
  - added iFDO.set_image_set_name_field_from_project_event_sensor()
  - none core item data for files that do not exist is now ignored instead of raising exception
  - fixed bug that existing camera-pose intermediate file was not added if already existed


## 0.5.0

  - adapting to iFDO 2.0.0 (breaking changes!)
    - added creation of image-set-[min,max]-[latitude,longitude]-degrees
    - image-depth and image-altitude replaced by image-altitude-meters
    - iFDO file format changed to json. Yaml files can still be read but will be saved as json.
    - added functionality to automatically upgrade from iFDO version < 2.0.0 (object with uri)
    - removed fairnessAcheived(). createiFDO will no throw exception if required field is missing unless allow_missing_required is expl. set true
    - adapted tryAutoSetHeaderFields to uri objects
    - removed isValidiFDOCoreHeader, use are_valid_ifdo_fields instead
    - removed isValidiFDOCapture and isValidiFDOContent, use validate_against_schema instead
    - removed isValidiFDOField, isValidiFDOItem, use is_valid_ifdo_field, are_valid_ifdo_fields instead
    - removed isValidiFDO, use validate_ifdo instead
    - renamed iFDO parameter startEmpty to ignore_image_files

## 0.4.2

  - added option pass additional item info file with custom datetime format

## 0.4.1

  - added create_fields() and update_fields() joining the functionality from createCoreFields() and createCaptureAndContentFields()

## 0.4.0

  - moved for_notebooks to separate package mariqt_widgets

## 0.3.0
  - Added required `image-local-path` field
  - Added `iFDOFromFile`
