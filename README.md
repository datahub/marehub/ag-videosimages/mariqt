> **Preamble:** We strive to make marine image data [FAIR](https://gitlab.hzdr.de/datahub/marehub/ag-videosimages/fair-marine-images/-/blob/master/docs/FAIR-marine-images.md). We develop [FDOs](https://gitlab.hzdr.de/datahub/marehub/ag-videosimages/fair-marine-images/-/blob/master/docs/ifdos/iFDO-overview.md) for images to establish a common language for marine imagery, we develop best-practice [operating procedures](https://gitlab.hzdr.de/datahub/marehub/ag-videosimages/fair-marine-images/-/blob/master/docs/sops/sop-overview.md) for handling marine images and - here in this repository - we develop [software tools](https://gitlab.hzdr.de/datahub/marehub/ag-videosimages/mariqt) to apply the vocabulary and procedures to marine imagery.

# MarIQT

Image Quality control / quality assurance and curation Tools (IQT) conceptualised and developed by the MareHub working group on Videos/Images (part of the DataHub, a research data manangement initiative by the Helmholtz association). The MarIQT core is a [python package](https://gitlab.hzdr.de/datahub/marehub/ag-videosimages/mariqt) which is helpful on its own but is key to the more user-friendly [jupyter notebooks](https://gitlab.hzdr.de/datahub/marehub/ag-videosimages/mariqt-notebooks) that make extensive use of the python package.

- Auto-generated documentation (CI) of MarIQT is available here: https://datahub.pages.hzdr.de/marehub/ag-videosimages/mariqt (password-protected)
- Public documentation is also available here: https://marine-imaging.com/mariqt (manually uploaded, probably older than previous link)

## Dependencies

MarIQT requires [python-xmp-toolkit](https://python-xmp-toolkit.readthedocs.io/en/latest/installation.html) which requires ```libxmp``` which might need to be installed with ```apt-get install libexempi-dev```.

## Installation

MarIQT is available on PyPI an can be installed with:

`pip install mariqt`

## Unit Tests

Run all unit tests:

- `pytest`
 
or:

- `python -m unittest -f tests/test_*`

## Creating iFDOs

In addition to the code example bellow check out the jupyter notebooks in [MarIQT Notebooks](https://codebase.helmholtz.cloud/datahub/marehub/ag-videosimages/mariqt-notebooks).


```python
import mariqt.directories as miqtd
import mariqt.sources.ifdo as miqtifdo
```

### Setup Image Directory

An iFDO object needs to be initialized with a directory object containing to a folder called *raw* (see [folder structure](https://marine-imaging.com/fair/sops/conventions/folder-structure/)) which contains your raw images.

**The image files need to contain UUIDs (v4)**

If they don't, see [Adding UUIDs to Image Files](#add-uuids-to-image-files).

**The image files need to comply with the file naming convention**: `<event>_<sensor>_<date>_<time>.<ext>`

where
  * the *event* format must be `<project>[-<project part>]_<event-id>[-<event-id-index>][_<device acronym>]`, e.g.: SO268_012-1
  * the *sensor* format must be `<owner>_<type>-<type index[_<subtype>[_<name>]]>`, e.g. GMR_CAM-01
  * the *date and time* (in **UTC**) formats must be `yyyymmdd` and `HHMMSS[.zzz]`, respectively
  * [..] is optional

In case your files do not comply with this convention, you can use the [Tomato](https://git.geomar.de/dsm/image-workflow/tomato-toolboxes/tomato-tools-info) Tool [FileRename](https://git.geomar.de/dsm/image-workflow/tomato-toolboxes/filerename) to rename you files accordingly.

If there is already an iFDO file in the *products* folder, this file will be loaded.


```python
images_dir = "..." # path to your raw images
miqt_dir = miqtd.Dir("",images_dir, create=False, with_gear=False)
ifdo = miqtifdo.iFDO(miqt_dir)
```

**Alternatively**, an iFDO object can be created from an explicit iFDO-file or, for header-only editing, without having to have the actual image files at hand.


```python
ifdo_file = "..."
ifdo = miqtifdo.ifdoFromFile(ifdo_file, ignore_image_files=True)
```

New field values are first written to a temporary, unchecked record and can be checked and set to the actual iFDO later on.

You can always check the current unchecked and checked ifdo:


```python
ifdo.getUnchecked()
```


```python
ifdo.getChecked()
```

### 1. Update/Provide Metadata

An iFDO consists of two parts: 
* a) the *Header* - containing information for the while data set
* b) the *Items* - containing individual information per image item. 

#### a) Header Information 


```python
header_fields = {
    'image-abstract': "500 - 2000 characters describing what, when, where, why and how the data was collected. Includes general information on the event (aka station, experiment), e.g. overlap between images/frames, parameters on platform movement, aims, purpose of image capture etc.",
    #'image-altitude-meters': "", # if empty will be automatically set representatively for respective item values 
    'image-context': {'name': "My cool project"},
    'image-coordinate-reference-system': "EPSG:4326",
    'image-coordinate-uncertainty-meters': 2,
    'image-copyright': "My copyright statement",
    'image-creators': [{'name': "Jane Doe", 'uri': "https://orcid.org/0000-0002-1825-0097"},
                       {'name': "John Doe", 'uri': "https://orcid.org/0000-0002-1825-0097"}],
    #'image-datetime': "", # if empty will be automatically set representatively for respective item values 
    'image-event': {'name': "POS123_003_AUV-01"},
    #'image-latitude': "", # if empty will be automatically set representatively for respective item values 
    'image-license': {'name': "CC-0"},
    #'image-longitude': "", # if empty will be automatically set representatively for respective item values 
    'image-pi': {'name': "Jane Doe", 'uri': "https://orcid.org/0000-0002-1825-0097"},
    'image-platform': {'name': "GMR_PFM-126_AUV_Sparus-II-ALBERT"},
    'image-project': {'name': "POS123"},
    'image-sensor': {'name': "GMR_CAM-1"},
    }
```

The information ca be used to *update* the (unchecked) iFDO record: 


```python
ifdo.updateHeaderFields(header_fields)
```

**Or** to *overwrite* it: 


```python
ifdo.setHeaderFields(header_fields)
```

Some fields can be auto-filled from others or external sources:


```python
ifdo.setImageSetNameFieldFromProjectEventSensor()
ifdo.trySetHeaderImageProjectUriToOsisExpeditionUrl()
ifdo.trySetHeaderImageEventUriToOsisEventUrl()
ifdo.trySetHeaderEquipmentUriToHandleUrl('image-sensor')
ifdo.trySetHeaderEquipmentUriToHandleUrl('image-platform')
ifdo.trySetLicenseUriFromLicenseName()
```

#### b) Items Information

For items information its convenient to compile the information in intermediate text table files first. 

##### Creating Intermediate Files

###### Start Times, UUIDs, Hashes (Required)


```python
msg = ifdo.createStartTimeFile()
msg = ifdo.createUuidFile()
msg = ifdo.createImageSha256File()
```

###### Navigation (Required)


```python
navigation_file =  "..." # path to your navigation data
nav_header = {  'utc': 'DateTime', 
                'lat': 'Latitude', 
                'lon': 'Longitude', 
                'dep': 'Depth',
                'hgt': 'Altitude', # optional
                #'uncert', 'Coordinate uncertainty', # optional\n",
                'yaw': 'Yaw', 'pitch': 'Pitch', 'roll': 'Roll' # only needed if lever arm compensation is to be done
                }
date_format = "%Y-%m-%d %H:%M:%S.%f"
offset_x,offset_y,offset_z = 0,1,0 # optional, lever arm offsets in meters in vehicle coordinates
video_sample_seconds = 1 # in case of video files every nth second navigation data is written
msg = ifdo.createImageNavigationFile(   navigation_file,
                                        nav_header=nav_header,
                                        date_format=date_format,
                                        overwrite=True,
                                        col_separator=",",
                                        video_sample_seconds=video_sample_seconds,
                                        offset_x=offset_x,offset_y=offset_y,offset_z=offset_z,angles_in_rad=False) # optional
```

###### Attitude (Optional)

Constant values for whole set:


```python
yaw_frame, pitch_frame, roll_frame = 0,0,0
yaw_cam2frame, pitch_cam2frame, roll_cam2frame = 0,0,0
ifdo.setImageSetAttitude(   yaw_frame,
                            pitch_frame,
                            roll_frame,
                            yaw_cam2frame,
                            pitch_cam2frame,
                            roll_cam2frame)
```

Time series values:


```python
frame_att_header = {'utc':   'DateTime', 
                    'yaw':   'Yaw', 
                    'pitch': 'Pitch', 
                    'roll':  'Roll'}
msg = ifdo.createImageAttitudeFile( navigation_file, 
                                    frame_att_header, 
                                    yaw_cam2frame, yaw_cam2frame, yaw_cam2frame,
                                    date_format, 
                                    overwrite=True, 
                                    col_separator=",",
                                    att_path_angles_in_rad=False,
                                    video_sample_seconds = video_sample_seconds)
```

###### Further Items Fields (Optional)

For other item fields you can provide your own intermediate files to be loaded. They need to contain the file name (and time for videos timestamps) and the corresponding iFDO field value.


```python
item_data_file = "..." # path to your item data file
header = {'image-filename':'image-filename', 'image-entropy':'entropy'}
ifdo.addItemInfoTabFile(item_data_file, separator=",", header=header)
```

Exif data per image can be extracted and added at once with:


```python
msg = ifdo.createAcquisitionSettingsExifFile()
```

### 2. Check Provided Information and Create/Update Fields

So far the information has been written to the unchecked iFDO record or to intermediate files only. Now you need to create/update the actual iFDO. 

**Update**

The newly provided header information and the loaded item data from the intermediate files is validated. If everything is fine the previously checked iFDO record (if any yet) is being updated with the new information. 


```python
ifdo.updateFields()
```

**Create** 

Same as *update* except that a previously checked iFDO record is not updated but **overwritten**.


```python
ifdo.createFields()
```

### 3. Write iFDO File


```python
ifdo.writeIfdoFile()
```

---
## Reading iFDOs

To read specific data from a iFDO object you use e.g. the following. If an item/item-timestamp does not contain a field but a superior field (header/item-default) does, the latter is returned.


```python
# Reading from checked iFDO record:
# raise exception if field not found
ifdo['POS123_003_AUV-01_GMR_CAM-1_20240320_111115.JPG:image-altitude-meters']
ifdo['POS123_003_AUV-01_GMR_CAM-1_20240320_111115.JPG:image-event']
ifdo.getCheckedValue('POS123_003_AUV-01_GMR_CAM-1_20240320_111115.JPG:image-event')
# returns empty string if field not found
ifdo.findCheckedValue('POS123_003_AUV-01_GMR_CAM-1_20240320_111115.JPG:image-event')

# Reading from unchecked iFDO record:
# raise exception if field not found
ifdo.getUncheckedValue('POS123_003_AUV-01_GMR_CAM-1_20240320_111115.JPG:image-event')
# returns empty string if field not found
ifdo.findUncheckedValue('POS123_003_AUV-01_GMR_CAM-1_20240320_111115.JPG:image-event')
```

Alternatively, you can use the `IfdoReader` to simply read from an iFDO file:


```python
ifdo_file = "..._iFDO.json"
ifdo_reader = miqtifdo.IfdoReader(ifdo_file)
```


```python
# raise exception if field not found
ifdo_reader['POS123_003_AUV-01_GMR_CAM-1_20240320_111115.JPG:image-altitude-meters']
# returns empty string if field not found
ifdo_reader.find('image-altitude-meters')
```


---
## Add UUIDs to Image Files

An essential part of the iFDO concept is that each image contains a UUID (Universally Unique Identifier) so it can be identified.

MarIQT supports writing UUIDs to following image types: mp4, mov, avi, mkv, wmv, jpg, png, jpeg, tif

For photos the UUID is written to the exif tag *ImageUniqueID*. For videos it is written to the *Xmp.dc.identifier* tag, except for mkv files where it is written to *SegmentUID*.


```python
import mariqt.image as miqti
```


```python
images_directory = "..."
search_subdirs_recursively = True

uuid_writer = miqti.UuidWriter(images_directory, search_subdirs_recursively=search_subdirs_recursively)
uuid_writer.browseDirectory()
uuid_writer.printStatusMessage()
```

The UUIDs can be written to a copy of the files, while keeping the original files in a renamed form as backup. Alternatively, you can add the UUIDs in place (`overwrite_originals`) but this should only be done if you have a **backup of your data**. The tools used do not guarantee integrity of modified files.

Optionally a UUID-adding-record-file can be created in the `images_directory` during process (`write_record_file`). Next to it you will find a python script that allows you to either remove the original files (if you didn't overwrite them) or undo the UUID-adding and restore the originals. 


```python
overwrite_originals = False # Caution!
write_record_file = True
uuid_writer.addMissingUUIDs()
```
