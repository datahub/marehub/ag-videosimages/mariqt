# this scripts updates the schema json files from fair-marine-images

echo "--> add repo"
git --work-tree=mariqt/resources/fair-marine-images remote add fair-marine-images git@codebase.helmholtz.cloud:datahub/marehub/ag-videosimages/fair-marine-images.git

echo "--> fetch"
git fetch fair-marine-images

echo "--> checkout"
git --work-tree=mariqt/resources/fair-marine-images checkout fair-marine-images/v2.0.0 docs/schemas/*.json