import unittest
import warnings
import requests

import mariqt.equipment as miqtequip

from tests.settings import OFFLINE


class Test_equipment(unittest.TestCase):

    def test_equipement_url(self):

        if OFFLINE:
            warnings.warn("Assert skipped as running in OFFLINE mode.")
            return 
        
        equipment = 'GMR_PFM-38_OFOS_XOFOS-Frame'
        url = miqtequip.equipment_url(equipment)
        requests.head(url)