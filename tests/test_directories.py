import unittest

import mariqt.directories as miqtd



class Test_directories(unittest.TestCase):

    def test_Dir_without_Gear(self):
        base_dir = "/base_path"
        project = "MSM96"
        event = "MSM96_003_AUV-01"
        sensor = "GMR_CAM-1"
        dir = project + "/" + event + "/" + sensor
        with_gear = False
        create = False

              
        miqtDir = miqtd.Dir(base_dir,dir,create,with_gear)
        
        self.assertEqual(project, miqtDir.project())
        self.assertEqual(event, miqtDir.event())
        self.assertEqual(sensor, miqtDir.sensor())
        self.assertEqual('/'.join([base_dir,project,event,sensor])+"/", miqtDir.tosensor())
        self.assertEqual('/'.join([base_dir,project,event,sensor])+"/", miqtDir.to(miqtDir.dp.SENSOR))
        self.assertEqual('/'.join([base_dir,project])+"/", miqtDir.to(miqtDir.dp.PROJECT))

        # without base dir
        dir = base_dir + "/" + dir + "/raw"
        miqtDir = miqtd.Dir("",dir,create=create,with_gear=with_gear)
        
        self.assertEqual(sensor, miqtDir.sensor())
        self.assertEqual(project, miqtDir.project())
        self.assertEqual(event, miqtDir.event())
        
        self.assertEqual('/'.join([base_dir,project,event,sensor])+"/", miqtDir.tosensor())
        self.assertEqual('/'.join([base_dir,project,event,sensor])+"/", miqtDir.to(miqtDir.dp.SENSOR))
        self.assertEqual('/'.join([base_dir,project])+"/", miqtDir.to(miqtDir.dp.PROJECT))


        self.assertEqual(dir.replace('raw','protocol/'),miqtDir.to(miqtDir.dt.protocol))
        self.assertEqual(dir.replace('raw','raw/'),miqtDir.to(miqtDir.dt.raw))
        self.assertEqual(dir.replace('raw','external/'),miqtDir.to(miqtDir.dt.external))
        self.assertEqual(dir.replace('raw','intermediate/'),miqtDir.to(miqtDir.dt.intermediate))
        self.assertEqual(dir.replace('raw','processed/'),miqtDir.to(miqtDir.dt.processed))
        self.assertEqual(dir.replace('raw','products/'),miqtDir.to(miqtDir.dt.products))


    def test_Dir_with_Gear(self):
        base_dir = "/base_path"
        project = "MSM96"
        gear = "OFOS-01"
        event = "MSM96_003_AUV-01"
        sensor = "GMR_CAM-1"
        dir = project + "/" + gear + "/" + event + "/" + sensor                   
        with_gear = True
        create = False               
        miqtDir = miqtd.Dir(base_dir,dir,create,with_gear)
        
        self.assertEqual(project, miqtDir.project())
        self.assertEqual(event, miqtDir.event())
        self.assertEqual(sensor, miqtDir.sensor())
        self.assertEqual('/'.join([base_dir,project,gear,event,sensor])+"/", miqtDir.tosensor())
        self.assertEqual('/'.join([base_dir,project,gear,event,sensor])+"/", miqtDir.to(miqtDir.dp.SENSOR))
        self.assertEqual('/'.join([base_dir,project])+"/", miqtDir.to(miqtDir.dp.PROJECT))

        # without base dir
        dir = base_dir + "/" + dir + "/raw"
        miqtDir = miqtd.Dir("",dir,create=create,with_gear=with_gear)
        
        self.assertEqual(project, miqtDir.project())
        self.assertEqual(event, miqtDir.event())
        self.assertEqual(sensor, miqtDir.sensor())
        self.assertEqual('/'.join([base_dir,project,gear,event,sensor])+"/", miqtDir.tosensor())
        self.assertEqual('/'.join([base_dir,project,gear,event,sensor])+"/", miqtDir.to(miqtDir.dp.SENSOR))
        self.assertEqual('/'.join([base_dir,project])+"/", miqtDir.to(miqtDir.dp.PROJECT))

    def test_findBaseForDir(self):
        base_dir_1 = "/home/karl/Nextcloud/HMC-Metadata/iFDO_testDataXYZ/"
        base_dir_2 = "/test"
        base_dirs = [base_dir_1, base_dir_2]
        dir = "MSM96"
        #print("yeah",miqtd.findBaseForDir(base_dirs,dir,True))
        #self.assertEqual(base_dir_1 + dir + "/",miqtd.findBaseForDir(base_dirs,dir,False).str())

