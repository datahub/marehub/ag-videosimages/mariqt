import unittest
import datetime
import math

import mariqt.converters as miqtconv
import mariqt.variables as miqtv



class Test_Converters(unittest.TestCase):

    def test_converters(self):

        conv = miqtconv.NoneConverter()
        self.assertTrue(conv.convert("abc") == "abc")

        conv = miqtconv.DateConverter()
        param = "%Y.%m.%d"
        conv.setParams(param)
        self.assertTrue(conv.getParams() == param) 
        self.assertTrue(conv.convert("2022.01.30") == "2022-01-30") 

        conv = miqtconv.TimeConverter()
        param = "%H:%M:%S"
        conv.setParams(param)
        self.assertTrue(conv.getParams() == param) 
        self.assertTrue(conv.convert("14:30:12") == "14:30:12.000000") 

        conv = miqtconv.DateTimeConverter()
        param = "%H:%M:%S %Y.%m.%d"
        conv.setParams(param)
        self.assertTrue(conv.getParams() == param) 
        self.assertTrue(conv.convert("14:30:12 2022.01.30") == datetime.datetime(2022,1,30,14,30,12).strftime(miqtv.date_formats["mariqt"])) 

        conv = miqtconv.ConstDoubleConverter()
        param = 12.3
        conv.setParams(param)
        self.assertTrue(conv.getParams() == param) 
        self.assertTrue(conv.convert("1") == "12.3") 

        conv = miqtconv.ConstDateConverter()
        param = "2022-01-30"
        conv.setParams(param)
        self.assertTrue(conv.getParams() == param) 
        self.assertTrue(conv.convert("1") == "2022-01-30") 

        conv = miqtconv.ConstTimeConverter()
        param = "14:30:12.0"
        conv.setParams(param)
        self.assertTrue(conv.getParams() == param) 
        self.assertTrue(conv.convert("1") == param)

        conv = miqtconv.ConstDateTimeConverter()
        param = "2022-01-30 14:30:12.0"
        conv.setParams(param)
        self.assertTrue(conv.getParams() == param) 
        self.assertTrue(conv.convert("1") == param) 

        conv = miqtconv.Rad2DegreeConvert()
        self.assertTrue(float(conv.convert(str(-math.pi))) == -180)

        conv = miqtconv.DoubleConstOffsetConverter()
        param = -4.1
        conv.setParams(param)
        self.assertTrue(float(conv.convert(str(5.1))) == 1.0)

        conv = miqtconv.DoubleMinMaxConverter()
        param = [1,5,0]
        conv.setParams(param)
        self.assertTrue(conv.convert(str(5.1)) is None)
        self.assertTrue(conv.convert(str(0.1)) is None)
        self.assertTrue(float(conv.convert(str(4.1))) == 4.1)
 