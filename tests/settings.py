import os

OFFLINE = False # tests requiring internet conneciton are skipped
TEST_BASE_DIR = os.path.dirname(os.path.abspath(__file__))
JSON_FLOAT_DECIMALS = 6 # only for unit test, so that produced test files do not depend on platform