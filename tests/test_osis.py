import unittest
import warnings
import requests

import mariqt.sources.osis as miqtosis
import mariqt.variables as miqtv

from tests.settings import OFFLINE


class Test_OSIS(unittest.TestCase):

    def test_get_osis_ids(self):

        if OFFLINE:
            warnings.warn("Test skipped as running in OFFLINE mode.")
            return

        exps = miqtosis.getExpeditionsFromLabel(label='SO268')
        assert len(exps) == 3

        exp_id = miqtosis.getExpeditionIdFromLabel('SO268/1')
        assert exp_id == 348623

        # pretty slow as it returns ALL expeditions
        #exp_ids = miqtosis.get_expedition_ids()
        #exp_id = exp_ids['SO268/1']
        #assert exp_id == 348623

        event_ids = miqtosis.getExpeditionEventIds(exp_id)
        event_id = event_ids["21-1"]
        assert event_id == 1603320

        exp_url = miqtosis.getExpeditionUrl(exp_id)
        requests.head(exp_url)

        event_url = miqtosis.getEventUrl(exp_id,event_id)
        requests.head(event_url)

        assert exp_id == miqtosis.getExpeditionIdFromUrl(event_url)
        assert -1 == miqtosis.getExpeditionIdFromUrl("www.test/test.test")


    def test_getOsisEventUrl(self):
        if OFFLINE:
            warnings.warn("Assert skipped as running in OFFLINE mode.")
            return

        event_url = miqtv.apis['osis_app'] + "expeditions/359211/events/1780794"
        exp_id = 359211
        assert event_url == miqtosis.getOsisEventUrl(exp_id,'3-1')
        assert event_url == miqtosis.getOsisEventUrl(exp_id,'03-1')
        assert event_url == miqtosis.getOsisEventUrl(exp_id,'03-01')
        assert event_url == miqtosis.getOsisEventUrl(exp_id,'M182_3-1_CTD')
        assert event_url == miqtosis.getOsisEventUrl(exp_id,'M182_3-01_CTD')
        assert event_url == miqtosis.getOsisEventUrl(exp_id,'M182_003-1_CTD')
        assert event_url == miqtosis.getOsisEventUrl(exp_id,'M182_003_CTD-01')
        assert "" == miqtosis.getOsisEventUrl(exp_id,'M182')
