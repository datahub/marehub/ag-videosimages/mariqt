import unittest
import copy

import mariqt.core as miqtc
import mariqt.tests as miqtt


class Test_tests(unittest.TestCase):

    def test_uuids(self):
        uuidValid = miqtc.uuid4()
        self.assertTrue(miqtt.isValidUuid(uuidValid))
        uuidFalse = "1234"
        self.assertFalse(miqtt.isValidUuid(uuidFalse))
        # see also test_validate_uuids


    def test_isValidImageName(self):
        self.assertFalse(miqtt.isValidImageName("with_UUID_20180208_173022.jpg")[0])
        self.assertTrue(miqtt.isValidImageName("MSM96_003_AUV-01_OI_CAM-01_19910910_133000.jpg")[0])
        self.assertTrue(miqtt.isValidImageName("MSM96_003_AUV-01_OI_CAM-01_19910910_133000.123.jpg")[0])
        self.assertTrue(miqtt.isValidImageName("with-uuid-MSM96_003_AUV-01_OI_CAM-01_19910910_133000.jpg")[0])
        self.assertTrue(miqtt.isValidImageName("ALKOR_000_GMR_CAM-78_20210603_071811.185.jpg")[0])
        self.assertFalse(miqtt.isValidImageName("ALKOR_000_GMR_COM-78_20210603_071811.185.jpg")[0])
        self.assertFalse(miqtt.isValidImageName("cruise_station_date_time.png")[0])
        #self.assertTrue(miqtt.isValidImageName("Albert-Test-2023-09-21_AUV-01_GMR_CAM-94_20230921_091047.JPG")[0])


    def test_isValidEventName(self):
        self.assertTrue(miqtt.isValidEventName("auvtest_000"))   


    def test_check_datetime_str_format(self):
        datetime_format = '%Y/%m/%d %H:%M:%S.%f'
        valid_datetime_str = "2019/03/04 08:37:24.000000"
        invalid_datetime_str = "2019-03-04 08:37:24.000000"
        miqtt.checkDatetimeStrFormat(valid_datetime_str, datetime_format)
        self.assertRaises(miqtc.IfdoException, miqtt.checkDatetimeStrFormat, invalid_datetime_str, datetime_format)


    def test_validateUuids(self):
        """ Version 4 UUIDs have the form xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx 
        where x is any hexadecimal digit and y is one of 8, 9, A, or B. """

        # the jsonschema "format": "uuid" does not allow for trimmed uuids (no dashes)
        schema_uuid_format = {
            "$schema": "https://json-schema.org/draft/2020-12/schema",
            "format": "uuid"
            }
        full = "a88c3fa8-0482-4620-a469-60fa98a4c5cc"
        trimmed = "a88c3fa804824620a46960fa98a4c5cc" 
        valid_full, msg = miqtt.validateAgainstSchema(full,schema=schema_uuid_format,short_msg=False)
        valid_trimmed, msg = miqtt.validateAgainstSchema(trimmed,schema=schema_uuid_format,short_msg=False)
        self.assertTrue(valid_full)
        self.assertFalse(valid_trimmed)
        
        # therefore it was replaced with this regex in version 2.0.1
        uuid_schema_regex = {
                "$schema": "https://json-schema.org/draft/2020-12/schema",
                "type": "string",
                "pattern": "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[4][0-9a-fA-F]{3}-[89abAB][0-9a-fA-F]{3}-[0-9a-fA-F]{12}$|^[0-9a-fA-F]{12}4[0-9a-fA-F]{3}[89abAB][0-9a-fA-F]{15}$"
                #           |\------------/                ---               --------                              ||
                #           |       |                       |                   |                                  ||
                #           |       |                       |                   variant must be one of 89abAB      ||
                #           |       |                       must be a 4 for version 4                              ||
                #           |       8 chars in range 0-9, a-f, A-F                                                 ||
                #           \_____________________________________________________________________________________ /|
                #             match length exactly                                                                  |
                #                                                                                                   or: same without dashes
                }

        # valid date regex against generated uuid 4s
        for i in range(1000):
            schema = uuid_schema_regex
            obj = str(miqtc.uuid4())
            # check version
            if obj[14] != "4":
                raise Exception('obj[14] != "4" ' + obj)
            # check variant
            if obj[19] not in ['8','9','a','b']:
                raise Exception(obj[19] not in ['8','9','a','b'])
            valid, msg = miqtt.validateAgainstSchema(obj,schema=schema,short_msg=False)
            if not valid:
                print(obj)
            self.assertTrue(valid)
        
        for uuid in self.getValidTestUuids():
            self.assertTrue(self.validateUuidAgainstCustomSchema(uuid, uuid_schema_regex))
            self.assertTrue(miqtt.isValidUuid(uuid))
            self.assertTrue(self.validateUuidAgainstIfdoSchema(uuid))

        for uuid in self.getInvalidTestUuids():
            self.assertFalse(self.validateUuidAgainstCustomSchema(uuid, uuid_schema_regex))
            self.assertFalse(miqtt.isValidUuid(uuid))
            self.assertFalse(self.validateUuidAgainstIfdoSchema(uuid))


    def validateUuidAgainstCustomSchema(self, uuid, schema):
        valid_schema, msg = miqtt.validateAgainstSchema(uuid,schema=schema,short_msg=False)
        return valid_schema
    

    def validateUuidAgainstIfdoSchema(self, uuid):
        valid_image_uuid = False
        valid_set_uuid = False

        try:
            miqtt.isValidIfdoField('image-uuid',uuid)
            valid_image_uuid = True
        except miqtc.IfdoException:
            pass

        try:
            miqtt.isValidIfdoField('image-set-uuid',uuid)
            valid_set_uuid = True
        except miqtc.IfdoException:
            pass
        return valid_image_uuid and valid_set_uuid


    def getValidTestUuids(self):
        valid_uuids = [
            "00000000-0000-4000-8000-000000000000",
            "00000000-0000-4000-9000-000000000000",
            "00000000-0000-4000-a000-000000000000",
            "00000000-0000-4000-b000-000000000000",
            "a88c3fa8-0482-4620-a469-60fa98a4c5cc",
            ]
        # add without dashes
        valid_uuids += [uuid.replace('-','') for uuid in valid_uuids]
        # add upper cases
        valid_uuids += [uuid.upper() for uuid in valid_uuids]
        return valid_uuids


    def getInvalidTestUuids(self):
        invalid_uuids = [
            "00000000-0000-0000-0000-000000000000",
            "a88c3fa8-0482-4620-a469-60fa98a4c5ccx", # too long
            "a88c3fa8-0482-4620-a469-60fa98a4c5c", # too short
            "g0000000-h000-4i00-8000-jklmnopqrstu", # invalid chars
            "g#000+00-h000-4i00-8000-jklmnopqrstu", # invalid chars
            "00000000-0000-4000-8000-00000000000ä" # invalid chars
            ]
        invalid_variants_letters = "cdefghijklmnopqrstuvwxyz"
        invalid_variants = "01234567" + invalid_variants_letters
        for i in range(len(invalid_variants)):
            invalid_uuids.append("00000000-0000-4000-" + invalid_variants[i] + "000-000000000000")
        # add without dashes
        invalid_uuids += [uuid.replace('-','') for uuid in invalid_uuids]
        # add upper cases
        invalid_uuids += [uuid.upper() for uuid in invalid_uuids]
        invalid_uuids.append(123)
        return invalid_uuids


    def test_isValidIfdoField(self):
        with self.assertRaises(Exception):
            value = miqtt.isValidIfdoField('image-uuid',"123")

        try:
            value = miqtt.isValidIfdoField('image-uuid',str(miqtc.uuid4()))
        except Exception as ex:
            self.fail(type(ex).__name__, ex.args)

        for uuid in self.getValidTestUuids():
            try:
                value = miqtt.isValidIfdoField('image-uuid',uuid)
            except Exception as ex:
                self.fail(type(ex).__name__, ex.args)

        for uuid in self.getInvalidTestUuids():
            with self.assertRaises(Exception):
                value = miqtt.isValidIfdoField('image-uuid',uuid)


    def test_areValidIfdoFields(self):
        item = {
            'image-uuid':"d08c85aa-0482-4620-a469-60fa98a4c5cc",#"a98f9b2407824620a46960fa98a4c5cc",#str(miqtc.uuid4()),
            'image-filename':"MSM96_003_AUV-01_OI_CAM-01_19910910_133000.jpg",
            'image-hash-sha256':"309a653580e21880f6663c5923f2fc654f7b55cd13625dfc75d8b9aa4976fd32",
            'image-datetime':'2018-02-08 17:30:22.123000',
            'image-longitude':112.2,
            'image-latitude':14.1,
            'image-depth':50,
            'image-coordinate-uncertainty-meters':0.7,
            'image-copyright':"geomar",
            'image-coordinate-reference-system':"EPSG",
            'image-sensor': {'name':"GMR_CAM-1"}
        }
        try:
            miqtt.areValidIfdoFields(item)
        except Exception as ex:
            self.fail(type(ex).__name__ + str(ex))

        # wrong type
        invalid_item = copy.deepcopy(item)
        invalid_item['image-latitude'] = '123.3'
        self.assertRaises(miqtc.IfdoException,miqtt.areValidIfdoFields,invalid_item)

        # missing required sub-field
        invalid_item = copy.deepcopy(item)
        invalid_item['image-sensor'] = {'abc':"GMR_CAM-1"}
        self.assertRaises(miqtc.IfdoException,miqtt.areValidIfdoFields,invalid_item)


    def test_isValidIfdoHeader(self):
        
        header = {'image-set-handle':"http://handle",
                  "image-coordinate-uncertainty-meters": 21.420907036655635}
        try:
            miqtt.areValidIfdoFields(header)
        except Exception as ex:
            self.fail(str( type(ex).__name__) + str(ex.args))

        header = {'image-set-handle':"/handle",
                  "image-coordinate-uncertainty-meters": -21.420907036655635}
        self.assertRaises(miqtc.IfdoException,miqtt.areValidIfdoFields,header)


    def test_orcidValid(self):
        orcid = "https://orcid.org/0000-0002-1825-0097"
        self.assertTrue(miqtt.isValidOrcid(orcid))
        orcid = "https://orcid.org/0000-0002-9079-593X"
        self.assertTrue(miqtt.isValidOrcid(orcid))
        orcid = "0000-0002-0035-3282"
        self.assertTrue(miqtt.isValidOrcid(orcid))

    # def test_isValidPerson(self):
    #     person = {'name' : "Jürgen M", 'email' : "j@mail.de", 'orcid' : "https://orcid.org/0000-0002-9079-593X"}
    #     self.assertTrue(miqtt.isValidPerson(person))
    #     person = {'name' : "Jürgen M", 'email' : "j mail.de", 'orcid' : "https://orcid.org/0000-0002-9079-593X"}
    #     self.assertFalse(miqtt.isValidPerson(person))
    #     person = {'name' : "Jürgen M", 'email' : "j@mail.de", 'orcid' : "https://orcid.org/0000-0002-9079-594X"}
    #     self.assertFalse(miqtt.isValidPerson(person))

