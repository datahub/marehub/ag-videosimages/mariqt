''' Collection of Unit Tests for MarIQT '''

import os
import copy
import unittest
from pprint import pprint
import yaml
from deepdiff import DeepDiff
import warnings
from freezegun import freeze_time

import mariqt.core as miqtc
import mariqt.variables as miqtv
import mariqt.sources.ifdo as miqtdiFDO
import mariqt.directories as miqtd
import mariqt.tests as miqtt

from tests.settings import OFFLINE, TEST_BASE_DIR, JSON_FLOAT_DECIMALS


# Freeze time for a pytest style test:
@freeze_time("2025-01-01", auto_tick_seconds=15)
class Test_iFDO(unittest.TestCase):

    def test_constructImageSetMame(self):
        project = "MSM128"
        event_short = "003-1_AUV"
        event_long = "MSM128_003-1_AUV"
        sensor = "GMR_CAM-1_Still_DeepSurveyCamera_Abyss"
        image_set_name_should = "MSM128_003-1_AUV_GMR_CAM-1_Still"
        assert image_set_name_should == miqtdiFDO.iFDO.constructImageSetName(project, event_short, sensor)
        assert image_set_name_should == miqtdiFDO.iFDO.constructImageSetName(project, event_long, sensor)


    def test_getLatLonBoundingBox(self):

        items ={'image-0':{ 'image-latitude': 10,
                            'image-longitude': 100},
                'image-1':{ 'image-latitude': -20,
                            'image-longitude': -120},
                'image-2':{ 'image-latitude': 0,
                            'image-longitude': 0},
                'image-3':{ },}
        lat_min, lat_max, lon_min, lon_max = miqtdiFDO.iFDO._getLatLonBoundingBox(items)
        assert lat_min == -20
        assert lat_max == 10
        assert lon_min == -120
        assert lon_max == 100


    def test_validateAgainstSchema(self):
        # format test
        uri = "https://marine-imaging.com/"
        valid, msg = miqtt.validateAgainstSchema(uri,{"type": "string","format": "uri"}, short_msg=True)
        assert valid

        # iFDO
        ifdo = {'$schema': 'https://marine-imaging.com/fair/schemas/ifdo-v2.0.0.json',
                'image-set-header': {'image-abstract': 'Acquired by camera SO_CAM-1_Photo_OFOS mounted on platform SO_PFM-01_OFOS during SO268-1_21-1_OFOS). Navigation data were automatically edited by the MarIQT software (removal of outliers, smoothed and splined to fill time gaps) and linked to the image data by timestamp.',
                                    #'image-acquisition': 'photo',
                                    'image-altitude-meters': -4094.5,
                                    #'image-capture-mode': 'mixed',
                                    'image-context': {  
                                        'name': 'Mining Impact of the Joint Programming Initiative of Healthy and Productive Seas and Oceans (JPIOceans)',
                                        #                'uri': 'https://miningimpact.geomar.de/de/miningimpact-2',
                                                        },
                                    'image-coordinate-reference-system': 'EPSG:4326',
                                    'image-coordinate-uncertainty-meters': 4.74,
                                    'image-copyright': '(c) GEOMAR Helmholtz Centre for Ocean Research Kiel. Contact: presse@geomar.de',
                                    'image-creators': [{'name': 'Yasemin Bodur',
                                                        'uri': 'https://orcid.org/0000-0001-8758-8617'
                                                        },
                                                        {'name': 'Timm Schoening',
                                                        'uri': 'https://orcid.org/0000-0002-0035-3282'}],
                                    #'image-curation-protocol': 'See provenance files for events',
                                    'image-datetime': '2019-03-04 08:37:24.000000',
                                    #'image-deployment': 'survey',
                                    'image-event': {'name': 'SO268-1_21-1_OFOS',
                                                    'uri': 'https://portal.geomar.de/metadata/event/show/1603320?leg=348623'},
                                    #'image-illumination': 'artificial light',
                                    #'image-item-identification-scheme': 'EVENT_SENSOR_DATE_TIME.jpg',
                                    'image-latitude': 11.9294422,
                                    'image-license': {'name': 'CC-BY',
                                                        'uri': 'https://creativecommons.org/licenses/by/4.0/legalcode'},
                                    'image-longitude': -117.0407515,
                                    #'image-marine-zone': 'seafloor',
                                    #'image-navigation': 'beacon',
                                    #'image-objective': 'Baseline imaging in nodule mining and reference areas. Some pre-/post-impact disturbance mapping',
                                    'image-pi': {'name': 'Timm Schoening',
                                                'uri': 'https://orcid.org/0000-0002-0035-3282'},
                                    'image-platform': {'name': 'SO_PFM-01_OFOS',
                                                        'uri': 'https://dm.pages.geomar.de/equipment/equipment/SO/PFM/SO_PFM-1_OFOS_Isitec/'},
                                    'image-project': {'name': 'SO268',
                                                        'uri': 'https://doi.org/10.3289/GEOMAR_REP_NS_59_20'},
                                    #'image-quality': 'raw',
                                    #'image-reference-calibration': 'None',
                                    #'image-resolution': 'mm',
                                    #'image-scale-reference': 'laser marker',
                                    'image-sensor': {'name': 'SO_CAM-1_Photo_OFOS',
                                                    'uri': 'https://dm.pages.geomar.de/equipment/equipment/SO/CAM/SO_CAM-1_Photo_OFOS/'},
                                    'image-set-handle': 'https://hdl.handle.net/20.500.12085/f840644a-fe4a-46a7-9791-e32c211bcbf5',
                                    'image-set-ifdo-version': 'v2.0.0',
                                    'image-set-name': 'SO268 SO268-1_21-1_OFOS SO_CAM-1_Photo_OFOS',
                                    'image-set-uuid': 'f840644a-fe4a-46a7-9791-e32c211bcbf5',
                                    #'image-spatial-constraints': 'Poly-metallic nodule areas in the Pacific Ocean',
                                    #'image-spectral-resolution': 'rgb',
                                    #'image-target-environment': 'Deep-sea floor, abyssal plains.',
                                    #'image-temporal-constraints': 'No specific temporal constraints, some pre-/post-impact disturbance mapping for a dredge experiment',
                                    #'image-time-synchronisation': 'Manual check and setting of the OFOS cameras to the correct UTC before the deployment'
                                    'image-set-provenance': {'provenance-agents':[{'name':'foo','id':'bar'}],
                                                             'provenance-entities':[{'name':'foo','id':'bar'}],
                                                             'provenance-activities':[{'start-time':"2023-05-26T13:13:00+00:00"}]
                                                             }
                                    },
                'image-set-items': 
                    {'SO268-1_21-1_OFOS_SO_CAM-1_20190304_083724.JPG': 
                                                                                        #[
                                                                                        {#'image-altitude-meters': -4094.5,
                                                                                        #'image-area-square-meters': 21.6994,
                                                                                        #'image-average-color': [168,187,144],
                                                                                        #'image-datetime': '2019-03-04 08:37:24.000000',
                                                                                        #'image-entropy': 0.8174957092476102,
                                                                                        'image-hash-sha256': '83f30eb35d1325c44c85fba0cf478825c0a629d20177a945069934f6cd07e087',
                                                                                        #'image-latitude': 11.9294422,
                                                                                        #'image-longitude': -117.0407515,
                                                                                        #'image-mpeg7-colorstatistic': [27.0,255.0,38.0,255.0, 36.0,255.0,169.3751983642578,211.97219848632812,198.2913360595703,25.55811309814453,31.31110954284668,37.18994140625,177.0,224.0,212.0, 0.8022223711013794,0.806853175163269,0.8450505137443542],
                                                                                        #'image-pixel-per-millimeter': 1.177876563911506,
                                                                                        'image-datetime': "2019-03-04 08:37:24.000000",
                                                                                        'image-uuid': 'c6b8d981-05c7-449f-85a9-906ab866bfb6',
                                                                                        'image-handle': "http://handle.net/",
                                                                                        },
                                    'SO268-1_21-1_OFOS_SO_CAM-1_20190304_083734.JPG': 
                                                                                        [
                                                                                            {#'image-altitude-meters': -4094.6,
                                                                                        #'image-area-square-meters': 29.6263,
                                                                                        #'image-average-color': [116,141,114],
                                                                                        #'image-datetime': '2019-03-04 08:37:34.000000',
                                                                                        #'image-entropy': 0.8340960692175852,
                                                                                        'image-hash-sha256': '819e1810dccd9025310d7841a994b94bcdc438ae74ec5095ce01f1bd46143c4e',
                                                                                        #'image-latitude': 11.9294418,
                                                                                        #'image-longitude': -117.040753,
                                                                                        #'image-pixel-per-millimeter': 1.0080566417738843,
                                                                                        'image-uuid': 'daad4a74-5c16-49d0-9775-efa67f0459f4',
                                                                                        'image-handle': "http://handle.net/"
                                                                                        },
                                                                                        {
                                                                                            'image-datetime': '2019-03-04 08:37:35.000000', 
                                                                                            'image-annotations': [{'shape':'circle','coordinates':[[1,1]],'labels':[],'frames':None}],
                                                                                            'image-annotation-labels': [{'id':"foo",'name':'bar'}],
                                                                                            'image-annotation-creators': [{'id':"foo",'name':'bar'}],
                                                                                        }
                                                                                        ]
                                                                                        }
                                                                                        #]
                                                                                        }
        valid, msg = miqtt.validateIfdo(ifdo, short_msg=False)
        #print(msg)
        assert valid

        # invalid file name
        invalid_ifdo = copy.deepcopy(ifdo)
        invalid_ifdo['image-set-items']['invalid_filename.jpggg'] = invalid_ifdo['image-set-items']['SO268-1_21-1_OFOS_SO_CAM-1_20190304_083724.JPG']
        valid, msg = miqtt.validateIfdo(invalid_ifdo, short_msg=False, check_file_names=True)
        #print("msg", msg)
        self.assertFalse(valid)
        valid, msg = miqtt.validateIfdo(invalid_ifdo, short_msg=False, check_file_names=False)
        self.assertTrue(valid)

        # invalid datetime format
        invalid_ifdo = copy.deepcopy(ifdo)
        invalid_ifdo['image-set-items'] = {'SO268-1_21-1_OFOS_SO_CAM-1_20190304_083724.JPG': invalid_ifdo['image-set-items']['SO268-1_21-1_OFOS_SO_CAM-1_20190304_083724.JPG']}
        invalid_ifdo['image-set-items']['SO268-1_21-1_OFOS_SO_CAM-1_20190304_083724.JPG']['image-datetime'] = "2019/03/04 08:37:24.000000"
        valid, msg = miqtt.validateIfdo(invalid_ifdo, short_msg=False, check_file_names=True)
        self.assertFalse(valid)
        invalid_ifdo['image-set-header']['image-datetime-format'] = '%Y/%m/%d %H:%M:%S.%f'
        valid, msg = miqtt.validateIfdo(invalid_ifdo, short_msg=False, check_file_names=True)
        self.assertTrue(valid)



    def test_IfdoReader(self):
        iFDOfile = TEST_BASE_DIR + "/TestFiles/test_write_world_files/ALKOR_000_GMR_CAM-77_Photo_CoraMo_iFDO.yaml"

        IfdoReader = miqtdiFDO.IfdoReader(iFDOfile)
        #pprint(IfdoReader.getImagesPositions())

        # "Python does some name mangling when it puts the actually-executed code together"
        self.assertTrue(IfdoReader._unitConversionFact2mm("12.5 mm test") == 1.0)
        self.assertTrue(IfdoReader._unitConversionFact2mm("um") == 1.0/1000)
        self.assertTrue(IfdoReader._unitConversionFact2mm("inches mm") == 25.4)
        self.assertTrue(IfdoReader._unitConversionFact2mm("123123m") == 1000)
        self.assertTrue(IfdoReader._unitConversionFact2mm("cm123") == 10)
        self.assertTrue(IfdoReader._unitConversionFact2mm("7.5") == 1)
        self.assertTrue(IfdoReader._unitConversionFact2mm("inchesmm") == -1)

        exif_str = "{Focal Length: '7.5 mm (35 mm equivalent: 18.5 mm)', Focal Plane Resolution Unit: mm, Focal Plane X Resolution: '289.855072021484', Focal Plane Y Resolution: '289.855072021484', GPS Altitude: 0 m Above Sea Level, GPS Altitude Ref: Below Sea Level, GPS Latitude: 0 deg 0' 0.00\" N, GPS Latitude Ref: North, GPS Longitude: 0 deg 0' 0.00\" E, GPS Longitude Ref: East, GPS Map Datum: WGS-84, GPS Position: '0 deg 0'' 0.00\" N, 0 deg 0'' 0.00\" E', GPS Processing Method: INS, GPS Time Stamp: '00:00:00', GPS Version ID: 2.2.0.0, Host Computer: hostname not yet known, Hyperfocal Distance: 1.32 m, ISO Speed: '125', Image Height: '3006', Image ID: '#project_#event_GMR_CAM-77_img000000_20220302_131724.905', Image Number: '', Image Size: 4104x3006, Image Unique ID: 603ce98f-27ca-4f76-85ca-bc1a56b35ec5, Image Width: '4104', JFIF Version: '1.01', Lens Make: lens manufacturer not yet known, Lens Model: Samyang 7.5mm F3.5 Fish-eye Lens, Lens Serial Number: lens serial number not yet known, MIME Type: image/jpeg, Make: Camera by IDS and System by AUV-Team of GEOMAR, Megapixels: '12.3', Original Raw File Name: '', Owner Name: AUV-Team GEOMAR, Resolution Unit: inches, Scale Factor To 35 mm Equivalent: '2.5'}"
        exif = yaml.safe_load(exif_str)
        #print(IfdoReader._tryDetermineFocalLenghtInPixelsFromExif_1(exif))
        #print(IfdoReader._tryDetermineFocalLenghtInPixelsFromExif_2(exif))
        focalLengthPix, msg = IfdoReader._tryDetermineFocalLenghtInPixelsFromExif_1(exif)
        self.assertTrue(abs(focalLengthPix[0] - 2173.9130401611296) < 0.00001)
        self.assertTrue(abs(focalLengthPix[1] - 2173.9130401611296) < 0.00001)
        focalLengthPix, msg = IfdoReader._tryDetermineFocalLenghtInPixelsFromExif_2(exif)
        self.assertTrue(abs(focalLengthPix[0] - 2109.0) < 0.00001)
        self.assertTrue(abs(focalLengthPix[1] - 2109.0) < 0.00001)

        item = "ALKOR_000_GMR_CAM-77_20210603_063724.905.jpg"
        #print(IfdoReader._tryGetFocalLengthInPixels(item,domePort=False))
        focalLengthPix, msg = IfdoReader._tryGetFocalLengthInPixels(item,domePort=False)
        self.assertTrue(abs(focalLengthPix[0] - 2891.3043434143024) < 0.00001)
        self.assertTrue(abs(focalLengthPix[1] - 2891.3043434143024) < 0.00001)
        domePort,msg = IfdoReader._tryGetIsDomePort(item)
        self.assertTrue(domePort == True)

        item = "ALKOR_000_GMR_CAM-77_20210603_063725.907.jpg"
        #print(IfdoReader._tryGetFocalLengthInPixels(item,domePort=True))
        focalLengthPix, msg = IfdoReader._tryGetFocalLengthInPixels(item,domePort=True)
        self.assertTrue(abs(focalLengthPix[0] - 123.4) < 0.00001)
        self.assertTrue(abs(focalLengthPix[1] - 432.1) < 0.00001)
        domePort,msg = IfdoReader._tryGetIsDomePort(item)
        self.assertTrue(domePort == None)

        destDir = TEST_BASE_DIR + "/TestFiles/test_write_world_files/"
        errors = IfdoReader.writeWorldFilesForPhotos(destDir,destDir)
        #print("errors",errors)
        self.assertTrue(errors == [])

        # get image directory
        ## does not contain image-set-local-path, so returns parent dir
        self.assertTrue(os.path.normpath("tests/TestFiles") == os.sep.join(os.path.normpath(IfdoReader.getImageDirectory()).split(os.sep)[-2:]))
        ## contains image-set-local-path
        path2sensor = "/TestFiles/testDir/MSM96/MSM96_003_AUV-01/GMR_CAM-1"
        iFDOfile = TEST_BASE_DIR + path2sensor + "/products/MSM96_003_AUV-01_GMR_CAM-1_iFDO.yaml"
        IfdoReader2 = miqtdiFDO.IfdoReader(iFDOfile)
        self.assertTrue(os.path.normpath(TEST_BASE_DIR + path2sensor + "/raw") == IfdoReader2.getImageDirectory())
        ## create fake iFDO file
        fake_iFDO = {miqtv.image_set_header_key:{'image-set-local-path': "root/sub1"},
                     miqtv.image_set_items_key: {   'item0':{},
                                                    'item1':{'image-set-local-path': "root/sub1/sub11"},
                                                    'item2':[{'image-set-local-path': "root/sub1/sub12"}]}}
        fake_iFDO_file = os.path.join(TEST_BASE_DIR,"fake_iFDO.yaml")
        o = open(fake_iFDO_file, "w")
        yaml.dump(fake_iFDO, o, default_style=None, default_flow_style=None, allow_unicode=True, width=float("inf"))
        o.close()
        IfdoReader3 = miqtdiFDO.IfdoReader(fake_iFDO_file)
        os.remove(fake_iFDO_file)
        self.assertTrue("root/sub1/",IfdoReader3.getImageDirectory())

        # test __getitem__
        test_iFDO = {miqtv.image_set_header_key:{   'image-test-key':1,
                                                    'image-header-key':3},
                     miqtv.image_set_items_key:{'test_item_1':[ 
                                    {'image-datetime':'2018-02-08 17:30:22.000000','image-test-key':2},
                                    {'image-datetime':'2018-02-08 17:30:23.000000'}]}}
        fake_iFDO_file = os.path.join(TEST_BASE_DIR,"fake_iFDO.yaml")
        o = open(fake_iFDO_file, "w")
        yaml.dump(test_iFDO, o, default_style=None, default_flow_style=None, allow_unicode=True, width=float("inf"))
        o.close()
        IfdoReader4 = miqtdiFDO.IfdoReader(fake_iFDO_file)
        os.remove(fake_iFDO_file)
        retVal = IfdoReader4['test_item_1']
        retValCorrect = {   '2018-02-08 17:30:22.000000': {'image-header-key':3,'image-test-key':2},
                            '2018-02-08 17:30:23.000000': {'image-header-key':3,'image-test-key':2}}
        self.assertTrue(retValCorrect == retVal)
        retVal = IfdoReader4['test_item_1:1']
        retValCorrect = {'image-datetime': '2018-02-08 17:30:23.000000','image-header-key':3,'image-test-key':2}
        self.assertTrue(retValCorrect == retVal)


    def test_ifdoFromFile(self):
        # exception case with images not in a valid data type dir
        ## create fake iFDO file
        fake_iFDO = {miqtv.image_set_header_key:{'image-set-local-path': "root/sub1----"},
                     miqtv.image_set_items_key: {   'item0':{},
                                                    'item1':{'image-set-local-path': "root/sub1/sub11"},
                                                    'item2':[{'image-set-local-path': "root/sub1/sub12"}]}}
        fake_iFDO_file = os.path.join(TEST_BASE_DIR,"fake_iFDO.yaml")
        o = open(fake_iFDO_file, "w")
        yaml.dump(fake_iFDO, o, default_style=None, default_flow_style=None, allow_unicode=True, width=float("inf"))
        o.close()
        self.assertRaises(miqtc.IfdoException, miqtdiFDO.ifdoFromFile,fake_iFDO_file,verbose=False)
        os.remove(fake_iFDO_file)

        # working excample
        iFDOfile = TEST_BASE_DIR + "/TestFiles/testDir/MSM96/MSM96_003_AUV-01/GMR_CAM-1/products/MSM96_003_AUV-01_GMR_CAM-1_processed_iFDO.yaml"
        iFDO = miqtdiFDO.ifdoFromFile(iFDOfile,verbose=False)


    def test_findPlainValue(self):
        testDict = {'key1':1,
                    'key2':{'key21':2},
                    'key3':[{'key31':3}]}
        ret = miqtdiFDO.iFDO._findPlainValue(testDict,'key1')
        self.assertTrue(ret == 1)
        ret = miqtdiFDO.iFDO._findPlainValue(testDict,['key2','key21'])
        self.assertTrue(ret == 2)
        ret = miqtdiFDO.iFDO._findPlainValue(testDict,['key3',0,'key31'])
        self.assertTrue(ret == 3)
        ret = miqtdiFDO.iFDO._findPlainValue(testDict,['key3','0','key31'])
        self.assertTrue(ret == 3)


    def test_getValue(self):

        # check return copy of list
        headerVal = ['header1','header2']
        test_iFDO = {miqtv.image_set_header_key:{'image-test-key':headerVal},
                     miqtv.image_set_items_key:{}}
        retVal = miqtdiFDO.iFDO._getValue(test_iFDO,'image-test-key')
        self.assertTrue(retVal == headerVal)
        retVal.append("header3")
        self.assertTrue(retVal != headerVal)

        # check return copy of dict
        headerVal = {'header1':'header2'}
        test_iFDO = {miqtv.image_set_header_key:{'image-test-key':headerVal},
                     miqtv.image_set_items_key:{}}
        retVal = miqtdiFDO.iFDO._getValue(test_iFDO,'image-test-key')
        self.assertTrue(retVal == headerVal)
        retVal["header1"] = "header3"
        self.assertTrue(retVal != headerVal)

        # check ignoring header and item prefixes
        headerVal = ['header1','header2']
        test_iFDO = {miqtv.image_set_header_key:{'image-test-key':headerVal},
                     miqtv.image_set_items_key:{}}
        retVal = miqtdiFDO.iFDO._getValue(test_iFDO,[miqtv.image_set_items_key,'image-test-key'])
        self.assertTrue(retVal == headerVal)

        # check getting default value
        headerVal = 'header1'
        test_iFDO = {miqtv.image_set_header_key:{'image-test-key':headerVal},
                     miqtv.image_set_items_key:{'test_item_1':[{}]}}
        retVal = miqtdiFDO.iFDO._getValue(test_iFDO,['test_item_1','0','image-test-key'])
        self.assertTrue(retVal == headerVal)
        test_iFDO = {miqtv.image_set_header_key:{'image-test-key':{'subKey1':headerVal}},
                     miqtv.image_set_items_key:{'test_item_1':[{'image-test-key':{'subKey2':'foo'}}]}}
        retVal = miqtdiFDO.iFDO._getValue(test_iFDO,['test_item_1','0','image-test-key','subKey1'])
        self.assertTrue(retVal == headerVal)

        # check getting default subfield
        headerVal = {'header1':{'header2':123}}
        test_iFDO = {miqtv.image_set_header_key:{'image-test-key':headerVal},
                     miqtv.image_set_items_key:{}}
        retVal = miqtdiFDO.iFDO._getValue(test_iFDO,['image-test-key','header1','header2'])
        self.assertTrue(retVal == 123)

        # check getting item value video 
        headerVal = 'header1'
        itemVal = "item"
        test_iFDO = {miqtv.image_set_header_key:{'image-test-key':headerVal},
                     miqtv.image_set_items_key:{'test_item_1':[{'image-test-key':itemVal}]}}
        retVal = miqtdiFDO.iFDO._getValue(test_iFDO,['test_item_1',0,'image-test-key'])
        self.assertTrue(retVal == itemVal)

        # check getting item value photo 
        headerVal = 'header1'
        itemVal = "item"
        test_iFDO = {miqtv.image_set_header_key:{'image-test-key':headerVal},
                     miqtv.image_set_items_key:{'test_item_1':{'image-test-key':itemVal}}}
        retVal = miqtdiFDO.iFDO._getValue(test_iFDO,['test_item_1','image-test-key'])
        self.assertTrue(retVal == itemVal)

        # check getting video default value
        headerVal = 'header1'
        itemVal_0 = "item_0"
        itemVal_1 = "item_1"
        test_iFDO = {miqtv.image_set_header_key:{'image-test-key':headerVal},
                     miqtv.image_set_items_key:{'test_item_1':[{'image-test-key':itemVal_0},
                                                               {'image-test-key':itemVal_1}]}}
        retVal = miqtdiFDO.iFDO._getValue(test_iFDO,['test_item_1','image-test-key'], default_only = True)
        self.assertTrue(retVal == itemVal_0)


        # check asking for not existing item
        headerVal = 'header1'
        test_iFDO = {miqtv.image_set_header_key:{'image-test-key':headerVal},
                     miqtv.image_set_items_key:{'test_item_1':[{}]}}
        self.assertRaises(miqtc.IfdoException, miqtdiFDO.iFDO._getValue,test_iFDO,['test_item_1----','0','image-test-key'])

        # check getting merged dicts
        itemVal = {'b':3,'c':4}
        headerVal = {'a':1,'b':2}
        test_iFDO = {miqtv.image_set_header_key:{'image-test-key':headerVal},
                     miqtv.image_set_items_key:{'test_item_1':[{'image-test-key':itemVal}]}}
        retVal = miqtdiFDO.iFDO._getValue(test_iFDO,['test_item_1','0','image-test-key'])
        self.assertTrue({'a':1,'b':3,'c':4} == retVal)


        # neglecing the index picture
        itemVal = {'b':3,'c':4}
        headerVal = {'a':1,'b':2}
        test_iFDO = {miqtv.image_set_header_key:{'image-test-key':headerVal},
                     miqtv.image_set_items_key:{'test_item_1':{'image-test-key':itemVal}}}
        retVal = miqtdiFDO.iFDO._getValue(test_iFDO,['test_item_1','image-test-key'])
        self.assertTrue({'a':1,'b':3,'c':4} == retVal)

        # neglecing the index video
        itemVal1 = 1 # default for video (at index 0) supersedes header default
        headerVal = 2
        test_iFDO = {miqtv.image_set_header_key:{'image-test-key':headerVal},
                     miqtv.image_set_items_key:{'test_item_1':[ 
                                    {'image-datetime':'2018-02-08 17:30:22.000000','image-test-key':itemVal1},
                                    {'image-datetime':'2018-02-08 17:30:23.000000'}]}}
        retVal = miqtdiFDO.iFDO._getValue(test_iFDO,['test_item_1','image-test-key'])
        retValCorrect = {   '2018-02-08 17:30:22.000000': itemVal1,
                            '2018-02-08 17:30:23.000000': itemVal1}
        self.assertTrue(retValCorrect == retVal)

        # neglecing the index video, dict
        itemVal1 = {'b':3,'c':4} # additional default for this video since at index 0
        itemVal2 = {'b':3,'d':5}
        headerVal = {'a':1,'b':2}
        test_iFDO = {miqtv.image_set_header_key:{'image-test-key':headerVal},
                     miqtv.image_set_items_key:{'test_item_1':[ 
                                    {'image-test-key':itemVal1,'image-datetime':'2018-02-08 17:30:22.000000'},
                                    {'image-test-key':itemVal2,'image-datetime':'2018-02-08 17:30:23.000000'}]}}
        retVal = miqtdiFDO.iFDO._getValue(test_iFDO,['test_item_1','image-test-key'])
        retValCorrect = {   '2018-02-08 17:30:22.000000': {'a': 1, 'b': 3, 'c': 4},
                            '2018-02-08 17:30:23.000000': {'a': 1, 'b': 3, 'c': 4, 'd': 5}}
        self.assertTrue(retValCorrect == retVal)

        # return also all values from header if key is complete picture item, with or without index
        test_iFDO = {miqtv.image_set_header_key:{   'image-test-key':1,
                                                    'image-header-key':3},
                     miqtv.image_set_items_key:{'test_item_1':{'image-test-key':2}}}
        retValCorrect = {'image-header-key':3,'image-test-key':2}
        retVal = miqtdiFDO.iFDO._getValue(test_iFDO,['test_item_1'])
        self.assertTrue(retValCorrect == retVal)

        # return also all values from header if key is complete video item, with or without index
        test_iFDO = {miqtv.image_set_header_key:{   'image-test-key':1,
                                                    'image-header-key':3},
                     miqtv.image_set_items_key:{'test_item_1':[ 
                                    {'image-datetime':'2018-02-08 17:30:22.000000','image-test-key':2},
                                    {'image-datetime':'2018-02-08 17:30:23.000000'}]}}
        retVal = miqtdiFDO.iFDO._getValue(test_iFDO,['test_item_1'])
        retValCorrect = {   '2018-02-08 17:30:22.000000': {'image-header-key':3,'image-test-key':2},
                            '2018-02-08 17:30:23.000000': {'image-header-key':3,'image-test-key':2}}
        self.assertTrue(retValCorrect == retVal)
        retVal = miqtdiFDO.iFDO._getValue(test_iFDO,['test_item_1','1'])
        retValCorrect = {'image-datetime': '2018-02-08 17:30:23.000000','image-header-key':3,'image-test-key':2}
        self.assertTrue(retValCorrect == retVal)

        # raise Exception if not found
        test_iFDO = {miqtv.image_set_header_key:{   'image-test-key':1},
                     miqtv.image_set_items_key:{'test_item_1':[ 
                                    {'image-datetime':'2018-02-08 17:30:22.000000','image-test-key-item':2}]}}

        self.assertRaises(miqtc.IfdoException,miqtdiFDO.iFDO._getValue,test_iFDO,['foo'])
        self.assertRaises(miqtc.IfdoException,miqtdiFDO.iFDO._getValue,test_iFDO,['test_item_1','image-test-key-item-foo'])


    def test_makePhotoItemsDictsIfLists(self):

        iFDO_test = { miqtv.image_set_items_key:{'test_item_1.jpg': [{'key_1': 1}],
                                                 'test_item_1.JPG': {'key_1': 1},
                                                 'test_item_1.mp4': [{'key_1': 1}]}
                    }
        iFDO_corrcet = { miqtv.image_set_items_key:{'test_item_1.jpg': {'key_1': 1},
                                                 'test_item_1.JPG': {'key_1': 1},
                                                 'test_item_1.mp4': [{'key_1': 1}]}
                    }
        miqtdiFDO.iFDO._makePhotoItemsDictIfListOfDicts(iFDO_test)
        self.assertTrue(iFDO_test == iFDO_corrcet)


    def test_getItemField(self):
        iFDOfile = TEST_BASE_DIR + "/TestFiles/testDir/MSM96/MSM96_003_AUV-01/GMR_CAM-1/products/MSM96_003_AUV-01_GMR_CAM-1_processed_iFDO.yaml"
        iFDO = miqtdiFDO.ifdoFromFile(iFDOfile,verbose=False)

        self.assertTrue(type(iFDO._ifdo_tmp["image-set-items"]["with-UUID-MSM96_003_AUV-01_GMR_CAM-1_20180208_173022.jpg"]) == dict)

        # non dict data
        itemValue = "test"
        headerValue = 2.1
        headerUpdate = {'image-test': headerValue}
        itemsUpdate = {'with-UUID-MSM96_003_AUV-01_GMR_CAM-1_20180208_173022.jpg':{'image-test':itemValue}}
        iFDO._updateAndCheck(headerUpdate,itemsUpdate)
        self.assertTrue(itemValue == iFDO['with-UUID-MSM96_003_AUV-01_GMR_CAM-1_20180208_173022.jpg:image-test'])

        # dict data
        itemValue = {'b':3,'c':4}
        headerValue = {'a':1,'b':2}
        headerUpdate = {'image-test': headerValue}
        itemsUpdate = {'with-UUID-MSM96_003_AUV-01_GMR_CAM-1_20180208_173022.jpg': {'image-test':itemValue}}
        iFDO._updateAndCheck(headerUpdate,itemsUpdate)
        self.assertTrue({'a':1,'b':3,'c':4} == iFDO['with-UUID-MSM96_003_AUV-01_GMR_CAM-1_20180208_173022.jpg:image-test'])
        self.assertTrue(8 == iFDO['with-UUID-MSM96_003_AUV-01_GMR_CAM-1_20180208_173022.jpg:image-acquisition-settings:Bits Per Sample'])


    def test_get_uris(self):
        if OFFLINE:
            warnings.warn("Assert skipped as running in OFFLINE mode.")
            return

        ifdo = {'image-set-header':{'image-project':{'name':'MSM96'},
                                    'image-event':{'name':'MSM96_045_OFOS-01'},
                                    'image-sensor':{'name':'GMR_PFM-5_AUV_REMUS6000-Abyss'},
                                    'image-license':{'name':'CC-0'}},
                'image-set-items':{}}
        project_uri_should = miqtv.apis['osis_app'] + "expeditions/357154"
        self.assertTrue(project_uri_should == miqtdiFDO.iFDO.getHeaderImageProjectUriToOsisExpeditionUrl(ifdo))

        event_uri_should = miqtv.apis['osis_app'] + "expeditions/357154/events/1763610"
        self.assertTrue(event_uri_should == miqtdiFDO.iFDO.getHeaderImageEventUriToOsisEventUrl(ifdo))

        eq_uri_should = "https://hdl.handle.net/20.500.12085/GMR_PFM-5"
        self.assertTrue(eq_uri_should == miqtdiFDO.iFDO.getEquipmentHandleUrl(ifdo,'image-sensor','https://hdl.handle.net/20.500.12085'))

        license_uri_should = "https://creativecommons.org/publicdomain/zero/1.0/legalcode"
        self.assertTrue(license_uri_should == miqtdiFDO.iFDO._getLicenseUriToLicense(ifdo))

        ifdo = {'image-set-header':{'image-license':{'name':'CC-By'}},
                'image-set-items':{}}
        license_uri_should = "https://creativecommons.org/licenses/by/4.0/legalcode"
        self.assertTrue(license_uri_should == miqtdiFDO.iFDO._getLicenseUriToLicense(ifdo))


    def test_getShortEquipmentID(self):
        equipmentLong = "GMR_PFM-5_AUV_REMUS6000-Abyss"
        self.assertTrue("GMR_PFM-5_AUV" == miqtdiFDO.iFDO.getShortEquipmentID(equipmentLong))
        equipmentLong = "GMR_CAM-1_Still_DeepSurveyCamera_Abyss"
        self.assertTrue("GMR_CAM-1_Still" == miqtdiFDO.iFDO.getShortEquipmentID(equipmentLong))


    def test_updateHeaderFields(self):
        base_dir = TEST_BASE_DIR + "/TestFiles/testDir/"
        dir = "MSM96/MSM96_003_AUV-01/GMR_CAM-1/raw/"
        miqtDir = miqtd.Dir(base_dir,dir,create=False,with_gear=False)
        iFDO = miqtdiFDO.iFDO(miqtDir,verbose=False)

        header1 = {
            'image-context':    {'name':"MarIQT Tests"},
            'image-abstract':   "The data was collected in the context of ___image-context:name___ and some more text, at least 500 chars. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eget imperdiet enim, eu dapibus quam. Nulla egestas eget massa sit amet iaculis. Aliquam velit est, finibus ac mauris vel, rutrum ornare libero. Nam quis ex vitae elit scelerisque rutrum eget faucibus enim. Vivamus suscipit risus nec odio imperdiet, non varius erat consectetur. Nulla hendrerit, odio vel varius molestie, justo justo luctus turpis, id suscipit risus quam ut urna. In dapibus ullamcorper porta. Curabitur non orci luctus turpis consectetur vulputate. Proin ipsum mi, cursus sed tincidunt in, malesuada sed nisi. Phasellus volutpat lorem nec volutpat tristique. In laoreet nunc eget leo.",
            'image-platform':   "AUV-01"}
        iFDO.setHeaderFields(header1)
        header2 = {
            'image-license':    {'name':"CC-BY"}}
        iFDO.updateHeaderFields(header2)
        header3 = {
            'image-context':    {'name':"MarIQT Tests"},
            'image-abstract':   "The data was collected in the context of ___image-context:name___ and some more text, at least 500 chars. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eget imperdiet enim, eu dapibus quam. Nulla egestas eget massa sit amet iaculis. Aliquam velit est, finibus ac mauris vel, rutrum ornare libero. Nam quis ex vitae elit scelerisque rutrum eget faucibus enim. Vivamus suscipit risus nec odio imperdiet, non varius erat consectetur. Nulla hendrerit, odio vel varius molestie, justo justo luctus turpis, id suscipit risus quam ut urna. In dapibus ullamcorper porta. Curabitur non orci luctus turpis consectetur vulputate. Proin ipsum mi, cursus sed tincidunt in, malesuada sed nisi. Phasellus volutpat lorem nec volutpat tristique. In laoreet nunc eget leo.",
            'image-platform':   "AUV-01",
            'image-license':    {'name':"CC-BY"}}
        self.assertTrue(iFDO._ifdo_tmp[miqtv.image_set_header_key] == header3)


    def test_createiFDO(self):
        base_dir = TEST_BASE_DIR + "/TestFiles/testDir/"
        dir = "MSM96/MSM96_003_AUV-01/GMR_CAM-1/raw/"
        miqtDir = miqtd.Dir(base_dir,dir,create=False,with_gear=False)
        iFDO = miqtdiFDO.iFDO(miqtDir,verbose=False,ignore_image_files=False)

        image_set_pi = {"name":         "Y. Bodur","email":        "ybod@rii.foo","orcid":        "0000-0002-1825-0097"}
        image_set_creators = [{"name":         "T. Bodur","email":        "tbod@rii.foo","orcid":        "0000-0002-1825-0097"},{"name":         "Y. Bodur","email":        "ybod@rii.foo","orcid":        "0000-0002-1825-0097"}]
        min_header = {
            'image-project':    {'name':"MSM96"},
            'image-event':      {'name':"MSM96_003_AUV-01"},
            'image-context':    {'name':"MarIQT Tests"},
            'image-abstract':   "The data was collected in the context of ___image-context:name___ and some more text, at least 500 chars. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eget imperdiet enim, eu dapibus quam. Nulla egestas eget massa sit amet iaculis. Aliquam velit est, finibus ac mauris vel, rutrum ornare libero. Nam quis ex vitae elit scelerisque rutrum eget faucibus enim. Vivamus suscipit risus nec odio imperdiet, non varius erat consectetur. Nulla hendrerit, odio vel varius molestie, justo justo luctus turpis, id suscipit risus quam ut urna. In dapibus ullamcorper porta. Curabitur non orci luctus turpis consectetur vulputate. Proin ipsum mi, cursus sed tincidunt in, malesuada sed nisi. Phasellus volutpat lorem nec volutpat tristique. In laoreet nunc eget leo.",
            'image-platform':   {'name':"AUV"},
            'image-sensor':     {'name':"GMR_CAM-1"},
            'image-set-local-path': "cool/path",
            'image-creators':   image_set_creators,
            'image-pi':         image_set_pi,
            'image-license':    {'name':"CC-BY"},
            'image-copyright':  "A copyright statement of who the data belongs to. Of whom to contact in case of data licensing or usage request.",
            'image-coordinate-reference-system':        "EPSG:4326",
            'image-coordinate-uncertainty-meters': 1.1,
            'image-set-name':"MSM96_003_AUV-01_GMR_CAM-1",
            'image-set-uuid': str(miqtc.uuid4()),
            'image-set-handle':"http://some/handle",
            'image-longitude': 123.4,
            'image-latitude':  12.3,
            'image-altitude-meters': 32.1,
            }

        items = {
                    #'invalid_filename.jpg':#
                    'with-UUID-MSM96_003_AUV-01_GMR_CAM-1_20180208_173022.jpg':
            {
                'image-uuid': str(miqtc.uuid4()),
                #'image-filename':"with-UUID-MSM96_003_AUV-01_GMR_CAM-1_20180208_173022.jpg",
                'image-hash-sha256':"24b656dfa7e61f803faf532adf5cbbd5f832d6e79c7eeee292da1581af2ecc1b",
                'image-datetime':"2000-01-01 00:00:00.0",
                'image-longitude':123.4,
                #'image-latitude':12.3,
                #'image-depth':42,
                'image-handle':"http://some/handle",
            }
        }

        update_existing = False

        ifdoFile = iFDO._createAndCheck(min_header, items,
                                    updateExisting=update_existing,
                                    allow_missing_required=False)

        # missing required field in header
        header_missing_required_field = copy.deepcopy(min_header)
        del header_missing_required_field['image-altitude-meters']
        self.assertRaises(miqtc.IfdoException, iFDO._createAndCheck, header_missing_required_field, items,
                                                                 updateExisting=update_existing,
                                                                 allow_missing_required=False)
        ifdoFile = iFDO._createAndCheck(header_missing_required_field, items,
                                    updateExisting=update_existing,
                                    allow_missing_required=True)

        # missing required field in item
        items_missing_required_field = copy.deepcopy(items)
        del items_missing_required_field['with-UUID-MSM96_003_AUV-01_GMR_CAM-1_20180208_173022.jpg']['image-uuid']
        self.assertRaises(miqtc.IfdoException, iFDO._createAndCheck, min_header, items_missing_required_field, 
                                                                 updateExisting=update_existing,
                                                                 allow_missing_required=False)
        ifdoFile = iFDO._createAndCheck(min_header, items_missing_required_field, 
                                    updateExisting=update_existing,
                                    allow_missing_required=True)
        
        # no items
        empty_items = {}
        header_with_datetime = copy.deepcopy(min_header)
        header_with_datetime['image-datetime'] = "2000-01-01 00:00:00.0"
        ifdoFile = iFDO._createAndCheck(header_with_datetime, empty_items, 
                                    updateExisting=update_existing,
                                    allow_missing_required=False)
        self.assertRaises(miqtc.IfdoException, iFDO._createAndCheck, min_header, empty_items, 
                                                                 updateExisting=update_existing,
                                                                 allow_missing_required=False)
        

        # ignore image files
        items_not_existing_file = copy.deepcopy(items)
        items_not_existing_file['FileDoesNotExist-UUID-MSM96_003_AUV-01_GMR_CAM-1_20180208_173022.jpg'] = \
            items['with-UUID-MSM96_003_AUV-01_GMR_CAM-1_20180208_173022.jpg']
        self.assertRaises(miqtc.IfdoException, iFDO._createAndCheck, min_header, items_not_existing_file, 
                                                                 updateExisting=update_existing,
                                                                 allow_missing_required=False)
        iFDO_ignore_files = miqtdiFDO.iFDO(miqtDir,verbose=False,ignore_image_files=True)
        import time
        time.sleep(1)
        ifdoFile = iFDO_ignore_files._createAndCheck(min_header, items_not_existing_file, 
                                    updateExisting=update_existing,
                                    allow_missing_required=False)



    def test_nonCoreFieldIntermediateItemInfoFile(self):
        a = miqtdiFDO.NonCoreFieldIntermediateItemInfoFile("fileA",",",{"a":1,"b":2})
        b = miqtdiFDO.NonCoreFieldIntermediateItemInfoFile("fileA",",",{"a":1,"b":2})
        self.assertTrue(a == b)
        self.assertFalse(a != b)

        liste = [a]
        self.assertTrue(b in liste)

        liste.remove(miqtdiFDO.NonCoreFieldIntermediateItemInfoFile("fileA",",",{"a":1,"b":2}))
        self.assertTrue(len(liste) == 0)


    def test_iFDO_explicit_file(self):
        base_dir = os.path.join(TEST_BASE_DIR, "TestFiles/testDir/")

        baseDir = os.path.join(base_dir, "MSM96/MSM96_003_AUV-01/GMR_CAM-1")
        rawDir = os.path.join(baseDir,"raw")
        miqtDir = miqtd.Dir("",rawDir, create=False, with_gear=False)
        self.assertRaises(miqtc.IfdoException, miqtdiFDO.iFDO,miqtDir,iFDOfile="I_dont_exist",verbose=False)
        iFDOfile = os.path.join(baseDir,"products/MSM96_003_AUV-01_GMR_CAM-1_iFDO.yaml")
        iFDO = miqtdiFDO.iFDO(miqtDir,iFDOfile=iFDOfile,verbose=False)


    def test_extractConstDataValues(self):
        # remove common entries and put in header
        data = {'image1':{'image-field_1':'foo',
                          'image-field_3':{'subfield_1':2}},
                'image2':{'image-field_1':'foo',
                          'image-field_3':{'subfield_1':2}}}
        header = {'image-field_2':0}
        miqtdiFDO.iFDO._extractConstDataValues(data,header)
        data_should = {'image1':{'image-field_1':'',
                                 'image-field_3':{'subfield_1':''}}, # should be emtpy string as it should overwrite potentally existing data
                       'image2':{'image-field_1':'',
                                 'image-field_3':{'subfield_1':''}}}
        header_should = {'image-field_2':0,
                         'image-field_1':'foo',
                         'image-field_3':{'subfield_1':2}}
        self.assertTrue(data_should == data)
        self.assertTrue(header_should == header)

        # case data already set
        data = {'image1':{'image-field_1':'foo',
                          'image-field_3':{'subfield_1':2}},
                'image2':{'image-field_1':'foo',
                          'image-field_3':{'subfield_1':2}}}
        header = {'image-field_1':'foo',
                  'image-field_3':{'subfield_1':2}}
        data_should = {'image1':{'image-field_1':'',
                                 'image-field_3':{'subfield_1':''}},
                       'image2':{'image-field_1':'',
                                 'image-field_3':{'subfield_1':''}}}
        header_should = copy.deepcopy(header)
        miqtdiFDO.iFDO._extractConstDataValues(data,header)
        self.assertTrue(data == data_should)
        self.assertTrue(header == header_should)

        # case would not add to header but overwrite
        data = {'image1':{'image-field_1':'foo',
                          'image-field_3':{'subfield_1':2}},
                'image2':{'image-field_1':'foo',
                          'image-field_3':{'subfield_1':2}}}
        header = {'image-field_1':1,
                  'image-field_3':{'subfield_1':2.1}}
        data_should = copy.deepcopy(data)
        header_should = copy.deepcopy(header)
        miqtdiFDO.iFDO._extractConstDataValues(data,header)
        self.assertTrue(data == data_should)
        self.assertTrue(header == header_should)

        # test DeepDiff still ueses same key words
        d1 = {'a':1}
        d2 = {'a':2}
        self.assertTrue('values_changed' in DeepDiff(d1,d2))
        d1 = {'a':1}
        d2 = {'a':'v'}
        self.assertTrue('type_changes' in DeepDiff(d1,d2))
        d1 = {'a':1}
        d2 = {}
        self.assertTrue('dictionary_item_removed' in DeepDiff(d1,d2))


    def test_writeParsedDataToItems(self):

        # TODO
        pass


    def test_tryUpgradeToVersion2(self):

        ifdo_file = TEST_BASE_DIR + "/TestFiles/testDir/MSM96/MSM96_003_AUV-01/GMR_CAM-1/products/MSM96_003_AUV-01_GMR_CAM-1_iFDO.yaml"
        iFDO = miqtdiFDO.ifdoFromFile(ifdo_file,verbose=False)
        iFDO.writeIfdoFile(float_decimals=JSON_FLOAT_DECIMALS)


    def test_iFDO(self):

        base_dir = os.path.join(TEST_BASE_DIR, "TestFiles/testDir/")
        dir = os.path.join(base_dir, "MSM96/MSM96_003_AUV-01/GMR_CAM-2/raw/")
        miqtDir = miqtd.Dir("",dir, create=False, with_gear=False)


        ### create iFDO from scratch
        self.clearIntermediateAndProduct()
        iFDO = miqtdiFDO.iFDO(miqtDir,verbose=False,sub_folders_ignore=['ignore_me'])

        # Header
        header_update = {   'image-abstract': 'The data was collected in the context of (context)...',
                            #'image-context': 'context',
                            #'image-coordinate-reference-system': 'EPSG:4326',
                            'image-coordinate-uncertainty-meters': 21.420907036655635,
                            #'image-copyright': 'Geomar',
                            #'image-creators':[{'email': 'm@geomar.de', 'name': 'Ina Mayer', 'orcid': '0000-0002-1825-0097'}],
                            #'image-event': 'MSM96_003_AUV-01',
                            #'image-license': 'CC-0',
                            #'image-set-local-path': '../raw',
                            'image-pi': {'name': 'Micha Müller ',},# 'orcid': '0000-0002-1825-0097'},
                            #'image-platform': 'AUV',
                            #'image-project': 'MSM96',
                            #'image-sensor': 'GMR_CAM-2'

                            # just to have no changes in test file
                            'image-set-uuid':'20736d5f-b232-45a3-abc0-9552e0c4f2e3',  # don't do this in a real application!
                            'image-set-handle': iFDO._handle_prefix + "/20736d5f-b232-45a3-abc0-9552e0c4f2e3" # don't do this in a real application!
                            }
        iFDO.updateHeaderFields(header_update)


        # create intermediate files
        msg = iFDO.createUuidFile()
        msg = iFDO.createStartTimeFile()
        msg = iFDO.createImageSha256File()

        # navigation
        ## position 
        navigation_file = os.path.join(base_dir, "MSM96/MSM96_003_AUV-01/GMR_Nav-01/navData.txt")
        nav_header = {'utc': 'Time', 
                      'lat': 'Latitude', 
                      'lon': 'Longitude', 
                      'dep': 'Depth',
                      'hgt': 'Altitude',
                      'yaw': 'Yaw', 
                      'pitch': 'Pitch', 
                      'roll': 'Roll' # only needed if leverarm compensation is to be done
                        }
        date_format = "%d.%m.%Y %H:%M:%S.%f"
        offset_x,offset_y,offset_z = 0,1,0 # optional, offsets in meters in vehicle coordinates
        videoSampleSeconds = 1 # in case of video files every nth second navigatin data is written
        msg = iFDO.createImageNavigationFile(   navigation_file,
                                                nav_header=nav_header,
                                                date_format=date_format,
                                                overwrite=True,
                                                col_separator="\t",
                                                video_sample_seconds=videoSampleSeconds,
                                                offset_x=offset_x,offset_y=offset_y,offset_z=offset_z,angles_in_rad=False) # optional

        ## attitude (optional)
        # const (in real case use either static or dynamic valies, not both!)
        yaw_frame, pitch_frame, roll_frame = 0,0,0
        yaw_cam2frame, yaw_cam2frame, yaw_cam2frame = 0,0,0
        iFDO.setImageSetAttitude(   yaw_frame,
                                    pitch_frame,
                                    roll_frame,
                                    yaw_cam2frame,
                                    yaw_cam2frame,
                                    yaw_cam2frame)
        # time series
        frame_att_header = {'utc': 'Time', 'yaw': 'Yaw', 'pitch': 'Pitch', 'roll': 'Roll'}
        msg = iFDO.createImageAttitudeFile( navigation_file, 
                                            frame_att_header, 
                                            yaw_cam2frame, yaw_cam2frame, yaw_cam2frame,
                                            date_format, 
                                            overwrite=True, 
                                            col_separator="\t",
                                            att_path_angles_in_rad=False,
                                            video_sample_seconds = videoSampleSeconds)

        ## add data per item from ascii file
        # static data (one value per item)
        itemData_file = os.path.join(base_dir, "MSM96/MSM96_003_AUV-01/GMR_CAM-2/external/nonCore_item_data_custom_field_stat.txt")
        header = {'image-filename':'image-filename', 'image-custom-field-stat':'image-custom-field-stat'}
        iFDO.addItemInfoTabFile(itemData_file,separator=",",header=header)
        # dynamic data (data per item per timestamp)
        itemData_file = os.path.join(base_dir, "MSM96/MSM96_003_AUV-01/GMR_CAM-2/external/nonCore_item_data_custom_field_dyn_1.txt")
        header = {'image-filename':'image-filename', 
                  'image-custom-field-dyn':'image-custom-field-dyn',
                  'image-custom-field-static':'image-custom-field-static',
                  'image-datetime': 'image-datetime'}
        iFDO.addItemInfoTabFile(itemData_file,separator=",",header=header)
        itemData_file = os.path.join(base_dir, "MSM96/MSM96_003_AUV-01/GMR_CAM-2/external/nonCore_item_data_custom_field_dyn_2.txt")
        header = {'image-filename':'image-filename', 
                  'image-custom-field-static-2':'image-custom-field-static-2',
                  'image-datetime': 'image-datetime'}
        iFDO.addItemInfoTabFile(itemData_file,separator=",",header=header, datetime_format='%Y/%m/%d %H%M%S.%f')

        # create Capture and Content fields
        msg = iFDO.createAcquisitionSettingsExifFile(override=True)

        iFDO.createFields(allow_missing_required=True)

        # write iFDO file
        iFDO.writeIfdoFile(float_decimals=JSON_FLOAT_DECIMALS)

        ### update iFDO 
        iFDO = miqtdiFDO.iFDO(miqtDir,verbose=False,sub_folders_ignore=['ignore_me'])

        # update header
        uncert_original = iFDO.getUncheckedValue('image-coordinate-uncertainty-meters')
        randuncert = 123.4 #random.uniform(0,100)
        header_update = {   'image-coordinate-uncertainty-meters':randuncert,
                            'image-abstract':   "The data was collected in the context of ___image-context:name___ ...",
                            'image-context': {'name':'ProjectXYZ'},
                            'image-coordinate-reference-system': 'EPSG:4326',
                            'image-copyright': 'AUV ABYSS Team / GEOMAR, Kiel\t',
                            'image-creators':[{'name': 'Foo Bar', 'orcid': 'https://orcid.org/0009-0003-5585-3350'}],
                            'image-event': {'name':'MSM96_003_AUV-01'},
                            #'image-event': {'name':'MSM96_003'},
                            'image-license': {'name':'CC-0'},#,'uri':'https://creativecommons.org/publicdomain/zero/1.0/legalcode'},
                            #'image-set-local-path': '../raw',
                            'image-pi': {'name': 'Foo Bar', 'orcid': 'https://orcid.org/0009-0003-5585-3350'},
                            'image-platform': {'name':'GMR_PFM-5_AUV_REMUS6000-Abyss'},
                            'image-project': {'name':'MSM96'},
                            'image-sensor': {'name':'GMR_CAM-2'},
                            'image-objective': 'Mapping (photomosaic) the research area XYZ in the CCZ at 11 m altitude.',
                            'image-deployment': 'mapping',
                            'image-target-environment': 'Research area XYZ in the CCZ',
                            'image-target-timescale': 'Photos taken at 1 Hz in 4 h.',
                            'image-temporal-constraints': 'Camera stopped working at 08.02.2018 17:31 UTC.'
                            }

        iFDO.updateHeaderFields(header_update)

        # get field value
        ## iFDO object contains two sets of fields:
        ## * temporary, unchecked fields from updating values before checking them by running createiFDO() as done in createCoreFields() and createCaptureAndContentFields()
        ## * checked fields
        self.assertTrue(randuncert == float(iFDO.getUncheckedValue('image-coordinate-uncertainty-meters')))        

        # remove field
        # TODO


        # auto set project and event uri from osis
        iFDO.trySetHeaderImageProjectUriToOsisExpeditionUrl(override=True)
        iFDO.trySetHeaderImageEventUriToOsisEventUrl(override=True)

        # set equipment uri
        iFDO.trySetHeaderEquipmentUriToHandleUrl('image-sensor',override=True)
        iFDO.trySetHeaderEquipmentUriToHandleUrl('image-platform',override=True)

        # set license uir
        iFDO.trySetLicenseUriFromLicenseName(override=True)

        iFDO.writeIfdoFile(float_decimals=JSON_FLOAT_DECIMALS)

        ## keys can be concatinated
        self.assertTrue(iFDO['image-set-header:image-platform:uri'] == "https://hdl.handle.net/20.500.12085/GMR_PFM-5")
        self.assertTrue(abs(9.86 - iFDO["MSM96_003_AUV-01_GMR_CAM-2_20180208_173022.jpg:image-camera-pitch-degrees"]) < 0.01)
        self.assertTrue("JPEG" == iFDO["MSM96_003_AUV-01_GMR_CAM-2_20180208_173022.jpg:image-acquisition-settings:File Type"])

        self.assertTrue(80 == iFDO["MSM96_003_AUV-01_GMR_CAM-2_20180208_173022.avi:0:image-acquisition-settings:Audio Sample Count"])
        video_audio_sample_count = {'2018-02-08 17:30:22.000000': 80, '2018-02-08 17:30:22.200000': 80, '2018-02-08 17:30:22.500000': 80, '2018-02-08 17:30:23.000000': 80}
        self.assertTrue(video_audio_sample_count ==  iFDO["MSM96_003_AUV-01_GMR_CAM-2_20180208_173022.avi:image-acquisition-settings:Audio Sample Count"])

        ### re-load iFDO and check updated values
        iFDO = miqtdiFDO.iFDO(miqtDir,verbose=False,sub_folders_ignore=['ignore_me'])
        self.assertTrue(randuncert == iFDO['image-coordinate-uncertainty-meters'])
        # just to have no changes in test file
        header_update = {   'image-coordinate-uncertainty-meters':uncert_original,
                            'image-set-uuid':'20736d5f-b232-45a3-abc0-9552e0c4f2e3',  # don't do this in a real application!
                            'image-set-handle': iFDO._handle_prefix + "/20736d5f-b232-45a3-abc0-9552e0c4f2e3" }  # don't do this in a real application!
                    

        iFDO.updateHeaderFields(header_update)
        iFDO.writeIfdoFile(float_decimals=JSON_FLOAT_DECIMALS)


        # # TODO
        # # schema = miqtv.ifdo_schema_reduced_for_field_validation
        # # fields = [e for e in schema['$defs']["iFDO-fields"]["properties"].keys()]
        # # fields.sort()
        # # for f in fields:
        # #     print('"'+f+'",')


        # test zipping
        iFDO.writeIfdoFile(as_zip = True, float_decimals=JSON_FLOAT_DECIMALS)
        iFDO2 = miqtdiFDO.ifdoFromFile(iFDO.getIfdoFileName() + ".zip",verbose=False,sub_folders_ignore=['ignore_me'])#,writeTmpProvFile=False)
        self.assertTrue(iFDO._ifdo_tmp == iFDO2._ifdo_tmp)


    def clearIntermediateAndProduct(self):
        # remove all content of /intermediate and /product subdirs in GMR_CAM-2
        base_dir = os.path.join(TEST_BASE_DIR, "TestFiles/testDir/MSM96/MSM96_003_AUV-01/GMR_CAM-2/")
        int_dir = os.path.join(base_dir,"intermediate")
        prod_dir = os.path.join(base_dir,"products")
        for file in os.listdir(int_dir):
            os.remove(os.path.join(int_dir,file))
        for file in os.listdir(prod_dir):
            os.remove(os.path.join(prod_dir,file))


    @classmethod
    def setUpClass(cls):
        """Tear down test fixtures, if any."""
        cls.clearProtocols()


    def clearProtocols():
        # remove provenance file to avoid a new provenance file is added after each test run
        base_dir = os.path.join(TEST_BASE_DIR, "TestFiles/testDir/")
        protocolPath = os.path.join(base_dir, "MSM96/MSM96_003_AUV-01/GMR_CAM-1/protocol/")
        prov_files = [f for f in os.listdir(protocolPath) if f.split('.')[-1] == "yaml"]
        for file in prov_files:
            os.remove(os.path.join(protocolPath,file))

        protocolPath = os.path.join(base_dir, "MSM96/MSM96_003_AUV-01/GMR_CAM-2/protocol/")
        prov_files = [f for f in os.listdir(protocolPath) if f.split('.')[-1] == "yaml"]
        for file in prov_files:
            os.remove(os.path.join(protocolPath,file))

    
