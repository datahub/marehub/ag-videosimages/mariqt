import unittest
import os
import shutil
from freezegun import freeze_time
import subprocess

import mariqt.image as miqti
import mariqt.files as miqtf
import mariqt.core as miqtc

from tests.settings import TEST_BASE_DIR


class Test_image(unittest.TestCase):

    def test_parseImageFileName(self):
        self.assertTrue(('',''),miqti.parseImageFileName("with_UUID_20180208_173022.jpg"))
        self.assertTrue(('MSM96_003_AUV-01', 'OI_CAM-01'),miqti.parseImageFileName("MSM96_003_AUV-01_OI_CAM-01_19910910_133000.jpg"))
        self.assertTrue(('MSM96_003_AUV-01', 'OI_CAM-01'),miqti.parseImageFileName("MSM96_003_AUV-01_OI_CAM-01_19910910_133000.123.jpg"))
        self.assertTrue(('with-uuid-MSM96_003_AUV-01', 'OI_CAM-01'),miqti.parseImageFileName("with-uuid-MSM96_003_AUV-01_OI_CAM-01_19910910_133000.jpg"))
        self.assertTrue(('ALKOR_000', 'GMR_CAM-78'),miqti.parseImageFileName("ALKOR_000_GMR_CAM-78_20210603_071811.185.jpg"))
        self.assertTrue(('',''),miqti.parseImageFileName("ALKOR_000_GMR_COM-78_20210603_071811.185.jpg"))


    def test_browseForImageFiles(self):
        myDir = TEST_BASE_DIR + "/TestFiles/"
        ret = miqti.browseForImageFiles(myDir)
        
        myDir = TEST_BASE_DIR + "/TestFiles/testDir/"
        ret = miqti.browseForImageFiles(myDir)
        #print("ret")
        #pprint(ret)
        self.assertTrue(len(ret) == 9)

        ret = miqti.browseForImageFiles(myDir,sub_folders_ignore=['GMR_CAM-2'])
        self.assertTrue(len(ret) == 4)

    
    def test_getVideoRuntime(self):
        myDir = TEST_BASE_DIR + "/TestFiles/test_videos/"
        testFiles = ["test_video.mp4","test_video.avi","test_video.mov","test_video.wmv","test_video.mkv" ,"/unsupported/test_video.MTS"]
        durations = [1.92,1.94,1.96,2.06,1.97,1.970022]
        i = 0
        for test_file in [myDir + f for f in testFiles]:
            duration = miqti.getVideoRuntime(test_file)
            self.assertTrue(duration == durations[i])
            i+=1
            

    def test_imageContainsValidUUID(self):
        self.assertFalse(miqti.imageContainsValidUUID(TEST_BASE_DIR + "/TestFiles/test_photos/without-UUID-MSM96_003_AUV-01_GMR_CAM-1_20180208_173022.jpg")[0])
        self.assertTrue(miqti.imageContainsValidUUID(TEST_BASE_DIR + "/TestFiles/test_photos/with-UUID-MSM96_003_AUV-01_GMR_CAM-1_20180208_173022.jpg")[0])

    def test_video_uuid(self):
        testVideosDir = TEST_BASE_DIR + "/TestFiles/test_videos/"
        uuid = ["1234-5678","e46f3d633efd3a5b1c17c40f22492246"] 
        testFiles = [os.path.join(testVideosDir,e) for e in os.listdir(testVideosDir) if os.path.isfile(os.path.join(testVideosDir,e))]
        for test_file in testFiles:
            uuid_read, msg = miqti.getVideoUUID(test_file)
            self.assertTrue(uuid_read in uuid, f"Failed to get video UUID from file {test_file}")

        # unsupported
        testVideosDir = TEST_BASE_DIR + "/TestFiles/test_videos/unsupported/"
        testFiles = [os.path.join(testVideosDir,e) for e in os.listdir(testVideosDir) if os.path.isfile(os.path.join(testVideosDir,e))]
        for test_file in testFiles:
            uuid_read, msg = miqti.getVideoUUID(test_file)
            self.assertTrue(uuid_read == "", f"Failed to get video UUID from file {test_file}")


    def test_createItemsDictFromList(self):
        items = [
            {'image-filename': "image1",'value1':1,'value2':2},
            {'image-filename': "image2",'value1':10,'value2':20},
        ]
        itemsDict = {
            "image1":{'image-filename': "image1",'value1':1,'value2':2},
            "image2":{'image-filename': "image2",'value1':10,'value2':20},
        }
        self.assertTrue(itemsDict == miqti.createImageItemsDictFromList(items))


    def test_getImageUUIDs(self):
        testPhotosDir = TEST_BASE_DIR + "/TestFiles/test_photos/"
        with_uuid = os.path.join(testPhotosDir, "with-UUID-MSM96_003_AUV-01_GMR_CAM-1_20180208_173022.jpg")
        uuid, all = miqti.getPhotoUUID(with_uuid)
        self.assertTrue(uuid == "82ec0082-b5bd-44ee-8b42-0547a36d7548")
        
        with_only_canon_uuid = os.path.join(testPhotosDir, "only_canon_unique_id.JPG")
        uuid, all = miqti.getPhotoUUID(with_only_canon_uuid)
        self.assertTrue(uuid == "")
        uuid, all = miqti.getPhotoUUID(with_only_canon_uuid, strict=False)
        self.assertTrue(uuid == "46a04180fb834620a46960fa98a4c5cc")
        
        ret = miqti.getImageUUIDsForFolder(testPhotosDir)
        self.assertTrue(ret["only_canon_unique_id.JPG"] == '-')


    def test_writeUUIDtoPhoto(self):
        testPhotosDir = TEST_BASE_DIR + "/TestFiles/test_photos/"
        testFiles = [os.path.join(testPhotosDir,e) for e in os.listdir(testPhotosDir) if os.path.isfile(os.path.join(testPhotosDir,e))]
        for testFile_orig in testFiles:
            testFile = miqtf.extendFileName(testFile_orig,"_test")
            shutil.copyfile(testFile_orig,testFile)
            uuid_pre, all = miqti.getPhotoUUID(testFile)
            miqti.writeUUIDtoPhoto(testFile, overwrite_original=True)
            uuid_post, all = miqti.getPhotoUUID(testFile)
            self.assertTrue(uuid_pre != uuid_post)
            # clean up
            os.remove(testFile)


    def test_writeUUIDtoVideo(self):
        testVideosDir = TEST_BASE_DIR + "/TestFiles/test_videos/"
        testFiles = [os.path.join(testVideosDir,e) for e in os.listdir(testVideosDir) if os.path.isfile(os.path.join(testVideosDir,e))]
        for testFile_orig in testFiles:
            testFile = miqtf.extendFileName(testFile_orig,"_test")
            shutil.copyfile(testFile_orig,testFile)
            uuid_pre, all = miqti.getVideoUUID(testFile)
            miqti.writeUUIDtoVideo(testFile, overwrite_original=True)
            uuid_post, all = miqti.getVideoUUID(testFile)

            self.assertTrue(uuid_pre != uuid_post)
            # clean up
            os.remove(testFile)


    def test_binaryMkvUuidToPlainTrimmedUuid(self):
        binary_uuid = "0xf8 0x60 0x28 0xd3 0xc1 0x86 0x4f 0xb3 0xb1 0x96 0x37 0xa4 0x86 0xa0 0xf2 0xf9"
        uuid_should = "f86028d3c1864fb3b19637a486a0f2f9"
        assert miqti.binaryMkvUuidToPlainTrimmedUuid(binary_uuid) == uuid_should


    def test_uuidToBinaryMkvUuid(self):
        uuid = "a88c3fa8-0482-7620-a469-60fa98a4c5cc"
        binary_uuid_should = "0xa8 0x8c 0x3f 0xa8 0x04 0x82 0x76 0x20 0xa4 0x69 0x60 0xfa 0x98 0xa4 0xc5 0xcc"
        assert miqti.uuidToBinaryMkvUuid(uuid) == binary_uuid_should


    # Freeze time for a pytest style test:
    @freeze_time("2025-01-01", auto_tick_seconds=15)
    def test_UuidWriter(self):
        dir_originals = TEST_BASE_DIR + "/TestFiles/test_write_uuids/originals/"
        dir_uuids_added = TEST_BASE_DIR + "/TestFiles/test_write_uuids/uuids_added/"
        
        if os.path.isdir(dir_uuids_added):
            shutil.rmtree(dir_uuids_added)
        shutil.copytree(dir_originals,dir_uuids_added)

        uuid_writer = miqti.UuidWriter(directory=dir_uuids_added, search_subdirs_recursively=True)

        uuid_writer.browseDirectory()
        #uuid_writer.printStatusMessage()

        assert len(uuid_writer.files_recognized) == 16
        assert len(uuid_writer.files_missing_uuid_supported) == 9
        assert len(uuid_writer.files_missing_uuid_unsupported) == 3

        uuid_writer.addMissingUUIDs(overwrite_originals=False, write_record_file=True)

        uuid_writer.browseDirectory()
        #uuid_writer.printStatusMessage()

        assert len(uuid_writer.files_recognized) == 16
        assert len(uuid_writer.files_missing_uuid_supported) == 0
        assert len(uuid_writer.files_missing_uuid_unsupported) == 3

        # restore originals 
        command = ["python", dir_uuids_added + "/post_uuid_adding.py", "-undo"]
        result = subprocess.run(command,stdout=subprocess.PIPE,stderr=subprocess.STDOUT,shell = miqtc.runningOnWindows())
        assert result.returncode == 0
