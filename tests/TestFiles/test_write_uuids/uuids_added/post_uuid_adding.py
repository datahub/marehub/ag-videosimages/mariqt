
""" This script allows bulk file editing after addind UUIDs to files using mariqt. 
	It requires a file record file created by the letter in the same directory (most recent is used).
	Executed with '-help' for more information """

import os
from datetime import datetime
import sys

tmp_newFilesListPrefix = "tmp_add_UUID_record_"
optionDelOrig = "-deleteOriginals"
optionUndo = "-undo"

# parse argument
if len(sys.argv) < 2:
	option = ""
else:
	option = sys.argv[1]
optionsHelp = ["","-help","--help"]
if option in optionsHelp:
	print("This script allows bulk file editing after addind UUIDs to files using mariqt. It requires a file \"" + tmp_newFilesListPrefix + "...\" in the same directory (most recent is used)")
	print("Arguments:")
	print(optionDelOrig,"(delete all original files)")
	print(optionUndo,"(delete all new files and rename the originals)")
	exit()

if option not in [optionDelOrig,optionUndo] + optionsHelp:
	print("unknown argument",option)
	exit()

tmp_newFilesFile = ""

# read tmp in same dir
myPath = os.path.dirname(os.path.realpath(__file__))
content = os.listdir(myPath)

def parseDateTimeFromFile(file):
	try:
		datetime_object = datetime.strptime(file[-19:-4], "%Y%m%d_%H%M%S")
		return datetime_object
	except ValueError:
		return datetime.min

# find most recent file
for cont in [os.path.join(myPath,e) for e in content]:
	if os.path.isfile(cont):
		if os.path.basename(cont)[0:len(tmp_newFilesListPrefix)] == tmp_newFilesListPrefix and parseDateTimeFromFile(cont) > parseDateTimeFromFile(tmp_newFilesFile):
			tmp_newFilesFile = cont

# read files
try:
	f = open(tmp_newFilesFile, "r")
	newFiles = f.readlines()
	f.close()
	origFiles = [os.path.join(myPath,e.split(",")[1].strip()) for e in newFiles]
	newFiles = [os.path.join(myPath,e.split(",")[0].strip()) for e in newFiles]
except:
	print("file " + tmp_newFilesListPrefix + " not found or invalid.")
	exit()

if option == optionDelOrig:
	i = 0
	for file in origFiles:
		i += 1
		print("deleting : " + str(i) +"/"+str(len(origFiles)) + "\t"+file, end="\r", flush=True)
		os.remove(file)
	exit()
if option == optionUndo:
	i = 0
	print("delete new files:")
	for file in newFiles:
		i += 1
		print("deleting : " + str(i) +"/"+str(len(origFiles)) + "\t"+file, end="\r", flush=True)
		os.remove(file)
	print("")
	print("Rename old files:")
	for i in range(len(origFiles)):
		print("renaming : " + str(i+1) +"/"+str(len(origFiles))+ "\t"+file, end="\r", flush=True)
		os.rename(origFiles[i], newFiles[i])
	print("")
	exit()
		