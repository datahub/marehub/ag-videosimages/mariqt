import unittest
import copy
import random
import numpy as np
import pymap3d
import math

import mariqt.navigation as miqtn
import mariqt.geo as miqtg
import mariqt.files as miqtf

class Test_navigation(unittest.TestCase):

    def test_latLon2UtmTrafo(self):
        lat = 14.1122621322489
        lon = -125.880989139303
        easting,northing,zone,isNorth = miqtg.latLon2utm(lat,lon)
        self.assertTrue(abs(easting - 188915.06625424) < 0.001)
        self.assertTrue(abs(northing - 1562050.1354749) < 0.001)
        self.assertTrue(zone == 10)
        self.assertTrue(isNorth == True)

        lat = -33.893217
        lon = 151.193848
        easting,northing,zone,isNorth = miqtg.latLon2utm(lat,lon)
        self.assertTrue(abs(easting - 332986.816) < 0.001)
        self.assertTrue(abs(northing - 6248215.564) < 0.001)
        self.assertTrue(zone == 56)
        self.assertTrue(isNorth == False)

    def test_median2D(self):
        x = [18.18425, 18.18425, 18.18425]
        y = [-22.185602, -22.185602, -22.185602]
        median,terminationCriterion = miqtg.median2D(x,y,maxIteratinos = 1000,TerminationTolerance = 0.0000001)
        #print(terminationCriterion)
        new_lat = median[0]
        new_lon = median[1]
        self.assertTrue(new_lat == x[0])
        self.assertTrue(new_lon == y[0])

    def test_convertImageNameToWorldFileName(self):
        self.assertTrue(miqtg.convertImageNameToWorldFileName("test.jpg") == "test.jgw")
        

    def test_invert_position(self):
        positions = miqtg.Positions()
        positions.setPos(miqtg.Position(160000000,1.00001,127.00001,4500,10))
        positions.setPos(miqtg.Position(160001000,1.00002,127.00002,4500,10))
        positions.invert_lat()
        positions.invert_lon()
        positions.invert_dep()
        positions.invert_hgt()
        self.assertTrue(positions[160000000] == miqtg.Position(1,-1.00001,-127.00001,-4500,-10))
        self.assertTrue(positions[160001000] == miqtg.Position(2,-1.00002,-127.00002,-4500,-10))


    def test_invert_attitude(self):
        attitudes = miqtg.Attitudes()
        attitudes.setAtt(miqtg.Attitude(160000000,1,2,3))
        attitudes.setAtt(miqtg.Attitude(160010000,1,2,3))
        attitudes.invert_yaw()
        attitudes.invert_pitch()
        attitudes.invert_roll()
        self.assertTrue(attitudes[160000000] == miqtg.Attitude(1,-1,-2,-3))
        self.assertTrue(attitudes[160010000] == miqtg.Attitude(2,-1,-2,-3))

    def test_removeSpatialOutliers(self):
        positions = miqtg.Positions()
        positions.setPos(miqtg.Position(160000000,1.00001,127.00001,4500))
        positions.setPos(miqtg.Position(160001000,1.00002,127.00002,4500))
        positions.setPos(miqtg.Position(160002000,1.00003,127.00003,4500))
        positions.setPos(miqtg.Position(160003000,1.00004,127.00004,4500))
        positions.setPos(miqtg.Position(160004000,1.00005,127.00005,4500))
        positions.setPos(miqtg.Position(160005000,1.00006,127.00006,4500))
        positions.setPos(miqtg.Position(160006000,1.00007,127.00007,4500))
        positions.setPos(miqtg.Position(160007000,1.00007,127.00008,4500))
        # outlier
        positions.setPos(miqtg.Position(160008000,1.00100,127.00100,4500))
        
        positions1 = copy.deepcopy(positions)
        numOutliers = miqtn.removeSpatialOutliers(positions1,min_neighbors=5,max_allowed_lateral_distance=10,max_allowed_vertical_distance=1,time_window_size=10)
        self.assertTrue(numOutliers == 1)

        positions2 = copy.deepcopy(positions)
        uncert = miqtn.smoothPositionsSlidingGaussian(positions2,gauss_half_width=10)


    def test_Position_linearInterpolateAt(self):
        positions_ = miqtg.Positions()
        positions_.setPos(miqtg.Position(150000000,None,None,None))
        positions_.setPos(miqtg.Position(160000000,1.00001,127.00001,4501))
        positions_.setPos(miqtg.Position(160001000,None,None,None,15))
        positions_.setPos(miqtg.Position(160002000,1.00003,127.00003,4503))
        positions_.setPos(miqtg.Position(160003000,1.00004,127.00004,4504))

        positionInterPolated, index = positions_.interpolateAtTime(160001200)

        #print(positionInterPolated.lat)
        #print(positionInterPolated.lon)
        #print(positionInterPolated.dep)

        self.assertTrue(abs(positionInterPolated.lat - 1.000022) < 0.000000001)
        self.assertTrue(abs(positionInterPolated.lon - 127.000022) < 0.000000001)
        self.assertTrue(abs(positionInterPolated.dep - 4502.2) < 0.000000001)

        positions_.interpolateNones()
        self.assertTrue(abs(positions_[160001000].lat - 1.00002) < 0.000000001)
        self.assertTrue(abs(positions_[160001000].lon - 127.00002) < 0.000000001)
        self.assertTrue(abs(positions_[160001000].dep - 4502) < 0.000000001)

        self.assertTrue(not 150000000 in positions_.keys())


    def test_Positions_toGeoJson(self):
        positions = miqtg.Positions()
        positions.setPos(miqtg.Position(150000000,None,None,None))
        positions.setPos(miqtg.Position(160000000,1.00001,127.00001,4501))
        positions.setPos(miqtg.Position(160001000,None,None,None,15))
        positions.setPos(miqtg.Position(160002000,1.00003,127.00003,4503))

        geojson_should = {'features': [{'geometry': {'coordinates': [127.00001, 1.00001, -4501],
                                                    'type': 'Point'},
                                    'properties': {'id': '1970-01-02 20:26:40.000000'},
                                    'type': 'Feature'},
                                    {'geometry': {'coordinates': [127.00003, 1.00003, -4503],
                                                    'type': 'Point'},
                                    'properties': {'id': '1970-01-02 20:26:42.000000'},
                                    'type': 'Feature'}],
                        'name': 'CollectionName',
                        'type': 'FeatureCollection'}

        geojson = positions.toGeoJsonPoints("CollectionName")
        #from pprint import pprint
        #pprint(geojson)
        assert geojson == geojson_should
        


    def test_Attitude(self):
        line = "2021-01-13 13:55:12.0,-10.0,1.2,-0.3"
        date_format = "%Y-%m-%d %H:%M:%S.%f"
        col_indcs = {'utc':0,'yaw':1,'pitch':2,'roll':3}
        a1 = miqtf.attitudeFromTabFileLine(line,col_indcs,date_format,add_utc=True,col_separator=',')
        a2 = miqtg.Attitude(miqtf.tryCreateUTCdateTimeMillisec("2021-01-13 13:55:12.0",date_format),-10,1.2,-0.3)
        self.assertTrue(a1 == a2)

        # interpolation
        a1 = miqtg.Attitude(100000500, 175, 5,10)
        a2 = miqtg.Attitude(100001000, 180,10,10)
        a3 = miqtg.Attitude(100001500,-175,15,10)
        attitudes = miqtg.Attitudes()
        attitudes.setAtt(a3)
        attitudes.setAtt(a1)
        aInt, ind = attitudes.interpolateAtTime(100001000)
        self.assertTrue(abs(aInt.yaw - a2.yaw) < 0.000000001)
        self.assertTrue(abs(aInt.pitch - a2.pitch) < 0.000000001)
        self.assertTrue(abs(aInt.roll - a2.roll) < 0.000000001)
        #print(aInt)

    def test_angularInterpolater(self):
        source_t = [4,1,2,3]
        source_x = [175,5,355,-175]
        interpolator = miqtg.AngularInterpolater(source_t,source_x,anglesInDegrees=True)
        #print("interpolator.interpolateAt(3.5)",interpolator.interpolateAt(3.5))
        self.assertTrue(abs(interpolator.interpolateAt(3.5) - 180)%360 < 1e-10)
        self.assertTrue(abs(interpolator.interpolateAt(1.5) -   0) < 1e-10)    


    def compare2Darray(self,a:np.array,b:np.array,max_diff:float=0):
        a_num_rows, a_num_cols = a.shape
        b_num_rows, b_num_cols = b.shape
        if a_num_rows != b_num_rows or a_num_cols != b_num_cols:
            return False
        for i in range(a_num_cols):
            for j in range(a_num_rows):
                if a[i,j] - b[i,j] > max_diff:
                    return False
        return True

    def test_compare2Darray(self):
        a = np.array([[1.0,2.0001],[3,5]])
        b = np.array([[1.0,2.0],[3,5]])
        self.assertTrue(self.compare2Darray(a,b,0.001))
        self.assertFalse(self.compare2Darray(a,b,0.00001))
        

    def test_rotation(self):
        for i in range(100):
            # single rotation
            yaw1 = random.uniform(-500,500)
            pitch1 = random.uniform(-500,500)
            roll1 = random.uniform(-500,500)
            R1 = miqtg.R_YawPitchRoll(yaw1,pitch1,roll1)
            # matrix from angles retrieved from first matrix (different set of angles can create same matrix)
            R1_1 = miqtg.R_YawPitchRoll(miqtg.yawPitchRoll(R1)[0],miqtg.yawPitchRoll(R1)[1],miqtg.yawPitchRoll(R1)[2])
            self.assertTrue(self.compare2Darray(R1,R1_1,0.0000001))

            # consecutive rotations
            yaw2 = random.uniform(-500,500)
            pitch2 = random.uniform(-500,500)
            roll2 = random.uniform(-500,500)
            R2 = miqtg.R_YawPitchRoll(yaw2,pitch2,roll2)
            R12 = R1.dot(R2)
            R3 = miqtg.R_YawPitchRoll(miqtg.yawPitchRoll(R12)[0],miqtg.yawPitchRoll(R12)[1],miqtg.yawPitchRoll(R12)[2])
            #print(R12,"\n",R3)
            self.assertTrue(self.compare2Darray(R12,R3,0.0000001))

            # consecutive different kind of rotations
            R2_2 = miqtg.R_XYZ(yaw2,pitch2,roll2)
            R4 = miqtg.R_YawPitchRoll(miqtg.yawPitchRoll(R2_2)[0],miqtg.yawPitchRoll(R2_2)[1],miqtg.yawPitchRoll(R2_2)[2])
            self.assertTrue(self.compare2Darray(R2_2,R4,0.0000001))

    def test_leveramOffset(self):

        orig_lat = 55.0
        orig_lon = 15.0
        orig_depth = 100
        orig_alt = 10

        # no offset
        offset_x = 0
        offset_y = 0
        offset_z = 0
        yaw = 0
        pitch = 0
        roll = 0
        new_lat,new_lon,new_depth,new_alt = miqtg.addLeverarms2LatLonDepAlt(orig_lat,orig_lon,orig_depth,orig_alt,offset_x,offset_y,offset_z,yaw,pitch,roll)
        self.assertTrue(abs(orig_lat-new_lat) < 1e-9)
        self.assertTrue(abs(orig_lon-new_lon) < 1e-9)
        self.assertTrue(abs(orig_depth-new_depth) < 1e-9)
        self.assertTrue(abs(orig_alt-new_alt) < 1e-9)

        # offest
        offset_x = 1
        offset_y = 1
        offset_z = 0
        yaw = 90
        pitch = -45
        roll = 0
        new_lat,new_lon,new_depth,new_alt = miqtg.addLeverarms2LatLonDepAlt(orig_lat,orig_lon,orig_depth,orig_alt,offset_x,offset_y,offset_z,yaw,pitch,roll)

        # print("")
        # print("ori_lat",orig_lat)
        # print("new_lat",new_lat)
        # print("ori_lon",orig_lon)
        # print("new_lon",new_lon)
        # print("ori_depth",orig_depth)
        # print("new_depth",new_depth)
        # print("orig_alt",orig_alt)
        # print("new_alt",new_alt)

        absOrig = math.sqrt(offset_x**2 + offset_y**2 + offset_z**2)
        for i in range(100):
            yaw = random.uniform(-360,360)
            pitch = random.uniform(-360,360)
            roll = random.uniform(-360,360)

            new_lat,new_lon,new_depth,new_alt = miqtg.addLeverarms2LatLonDepAlt(orig_lat,orig_lon,orig_depth,orig_alt,offset_x,offset_y,offset_z,yaw,pitch,roll)
            ned_diff = pymap3d.geodetic2ned(orig_lat,orig_lon,orig_depth,new_lat,new_lon,new_depth)
            absNew = math.sqrt(ned_diff[0]**2 + ned_diff[1]**2 + ned_diff[2]**2)
            self.assertTrue(abs(absNew-absOrig) < 1e-4) 
            #if absNew > absOrig:
            #print(absNew,absOrig,abs(absNew-absOrig))

