import unittest
import mariqt.files as miqtf


class Test_files(unittest.TestCase):

    def test_extendFileName(self):
        oldFileName = "/path/fileName.txt"
        newFileName = "/path/fileName_new.txt"
        self.assertTrue(miqtf.extendFileName(oldFileName,"_new") == newFileName)

    def test_changeFileExtension(self):
        oldFileName = "/path/fileName.txt"
        newFileName = "/path/fileName.csv"
        self.assertTrue(miqtf.changeFileExtension(oldFileName,'csv') == newFileName)