import unittest
import datetime
import json
import copy

import mariqt.variables as miqtv
import mariqt.core as miqtc


class Test_core(unittest.TestCase):
    
    def test_roundFloats(self):
        a = {'a': "test",
             'b': 2.12345679,
             'c': [2.12345679, 2.12345679, "test"],
             }
        a['d'] = copy.deepcopy(a)
        a_rounded_4_should = {'a': "test",
                     'b': 2.1235,
                     'c': [2.1235, 2.1235, "test"],
                    }
        a_rounded_4_should['d'] = copy.deepcopy(a_rounded_4_should)

        a_rounded_4 = miqtc.roundFloats(a, decimals=4)
        
        assert a_rounded_4 == a_rounded_4_should
        assert a != a_rounded_4
        

    def test_parseFileDateTimeAsUTC(self):
        self.assertEqual(miqtc.parseFileDateTimeAsUTC("with_UUID_20180208_173022.jpg"), datetime.datetime(2018,2,8,17,30,22,0,tzinfo=datetime.timezone.utc))
        self.assertEqual(miqtc.parseFileDateTimeAsUTC("with_UUID_20180208_173022.123.jpg"), datetime.datetime(2018,2,8,17,30,22,123000,tzinfo=datetime.timezone.utc))
        with self.assertRaises(Exception):
            miqtc.parseFileDateTimeAsUTC("with_UUID.jpg")
        with self.assertRaises(Exception):
            miqtc.parseFileDateTimeAsUTC("with_UUID_20180208_17302u.jpg")

    
    def test_identicalMariqtDatetime(self):
        assert miqtc.identicalMariqtDatetime('2018-02-08 17:30:23.000000', '2018-02-08 17:30:23.000000')
        assert miqtc.identicalMariqtDatetime('2018-02-08 17:30:23.000000', '2018-02-08 17:30:23.0')
        assert not miqtc.identicalMariqtDatetime('2018-02-08 17:30:23.000000', '2018-02-08 17:30:23.000001')
    
    def test_recursivelyUpdateDicts(self):
        
        dict1 = {'a':{'a1':1,'a2':2},'b':{'b1':2}}
        dict2 = {'c':{'c1':3},'a':{'a1':10}}
        dict3 = {'a':{'a1':10,'a2':2},'b':{'b1':2},'c':{'c1':3}}
        miqtc.recursivelyUpdateDicts(dict1,dict2)
        self.assertTrue(dict1 == dict3)

        dict1 = {'a':{'a1':1},'b':{'b1':2}}
        dict2 = {'c':{'c1':3},'a':{'a1':{'a2':10}}}
        dict3 = {'a':{'a1':{'a2':10}},'b':{'b1':2},'c':{'c1':3}}
        miqtc.recursivelyUpdateDicts(dict1,dict2)
        self.assertTrue(dict1 == dict3)

        dict1 = {}
        dict2 = {'c':{'c1':3},'a':{'a1':{'a2':10}}}
        dict3 = {'c':{'c1':3},'a':{'a1':{'a2':10}}}
        miqtc.recursivelyUpdateDicts(dict1,dict2)
        self.assertTrue(dict1 == dict3)

        ### containing items as list (videos)
        # same timestamp
        dict1 = {'header-field':{'a1':1},'item':{'image':{"field":3},'video1':{'image-datetime': '2018-02-08 17:30:23.000',"fieldConst":4}}}
        dict2 = {'header-field':{'a1':1},'item':{'image':{"field":3},'video1':{'image-datetime': '2018-02-08 17:30:23.000000',"fieldDiff":40}}} 
        dict3 = {'header-field':{'a1':1},'item':{'image':{"field":3},'video1':{'image-datetime': '2018-02-08 17:30:23.000000',"fieldConst":4,"fieldDiff":40}}}
        miqtc.recursivelyUpdateDicts(dict1,dict2)
        self.assertTrue(dict1 == dict3)

        # different timestamp
        dict1 = {'header-field':{'a1':1},'item':{'image':{"field":3},'video1':{'image-datetime': '2018-02-08 17:30:23.000000',"fieldConst":4}}}
        dict2 = {'header-field':{'a1':1},'item':{'image':{"field":3},'video1':{'image-datetime': '2018-02-08 17:30:24.000000',"fieldDiff":40}}}
        dict3 = {'header-field':{'a1':1},'item':{'image':{"field":3},'video1':[ {'image-datetime': '2018-02-08 17:30:23.000000',"fieldConst":4},
                                                                                {'image-datetime': '2018-02-08 17:30:24.000000',"fieldDiff":40}]}}
        miqtc.recursivelyUpdateDicts(dict1,dict2)
        self.assertTrue(dict1 == dict3)

        # already in list(s)
        dict1 = {'header-field':{'a1':1},'item':{'image':{"field":3},'video1':[{'image-datetime': '2018-02-08 17:30:23.000000',"fieldConst":4}]}}
        dict2 = {'header-field':{'a1':1},'item':{'image':{"field":3},'video1':{'image-datetime': '2018-02-08 17:30:24.000000',"fieldDiff":40}}} 
        dict3 = {'header-field':{'a1':1},'item':{'image':{"field":3},'video1':[ {'image-datetime': '2018-02-08 17:30:23.000000',"fieldConst":4},
                                                                                {'image-datetime': '2018-02-08 17:30:24.000000',"fieldDiff":40}]}}
        miqtc.recursivelyUpdateDicts(dict1,dict2)
        self.assertTrue(dict1 == dict3)

        dict1 = {'header-field':{'a1':1},'item':{'image':{"field":3},'video1':{'image-datetime': '2018-02-08 17:30:23.000000',"fieldConst":4}}}
        dict2 = {'header-field':{'a1':1},'item':{'image':{"field":3},'video1':[{'image-datetime': '2018-02-08 17:30:24.000000',"fieldDiff":40}]}} 
        dict3 = {'header-field':{'a1':1},'item':{'image':{"field":3},'video1':[ {'image-datetime': '2018-02-08 17:30:23.000000',"fieldConst":4},
                                                                                {'image-datetime': '2018-02-08 17:30:24.000000',"fieldDiff":40}]}}
        miqtc.recursivelyUpdateDicts(dict1,dict2)
        self.assertTrue(dict1 == dict3)

        dict1 = {'header-field':{'a1':1},'item':{'image':{"field":3},'video1':[{'image-datetime': '2018-02-08 17:30:23.000000',"fieldConst":4}]}}
        dict2 = {'header-field':{'a1':1},'item':{'image':{"field":3},'video1':[{'image-datetime': '2018-02-08 17:30:24.000000',"fieldDiff":40}]}} 
        dict3 = {'header-field':{'a1':1},'item':{'image':{"field":3},'video1':[ {'image-datetime': '2018-02-08 17:30:23.000000',"fieldConst":4},
                                                                                {'image-datetime': '2018-02-08 17:30:24.000000',"fieldDiff":40}]}}
        miqtc.recursivelyUpdateDicts(dict1,dict2)
        self.assertTrue(dict1 == dict3)

        # no datetime in update
        dict1 = {'header-field':{'a1':1},'item':{'image':{"field":3},'video1':{'image-datetime': '2018-02-08 17:30:23.000000',"fieldConst":4}}}
        dict2 = {'header-field':{'a1':1},'item':{'image':{"field":3},'video1':{"fieldDiff":{'val':40}}}}
        dict3 = {'header-field':{'a1':1},'item':{'image':{"field":3},'video1':{'image-datetime': '2018-02-08 17:30:23.000000',"fieldConst":4,"fieldDiff":{'val':40}}}}
        miqtc.recursivelyUpdateDicts(dict1,dict2)
        self.assertTrue(dict1 == dict3)

        dict1 = {'header-field':{'a1':1},'item':{'image':{"field":3},'with-UUID-MSM96_003_AUV-01_GMR_CAM-1_20180208_173022.jpg':[{'image-datetime': '2018-02-08 17:30:22.000000',"fieldConst":4}]}}
        dict2 = {'header-field':{'a1':1},'item':{'image':{"field":3},'with-UUID-MSM96_003_AUV-01_GMR_CAM-1_20180208_173022.jpg':{"fieldDiff":{'val':40}}}}
        dict3 = {'header-field':{'a1':1},'item':{'image':{"field":3},'with-UUID-MSM96_003_AUV-01_GMR_CAM-1_20180208_173022.jpg':[ {'image-datetime': '2018-02-08 17:30:22.000000',"fieldConst":4,"fieldDiff":{'val':40}}]}}
        miqtc.recursivelyUpdateDicts(dict1,dict2)
        self.assertTrue(dict1 == dict3)

        # sort by time
        dict1 = {'header-field':{'a1':1},'item':{'image':{"field":3},'video1':[{'image-datetime': '2018-02-08 17:30:24.000000',"fieldDiff":40}]}}
        dict2 = {'header-field':{'a1':1},'item':{'image':{"field":3},'video1':[{'image-datetime': '2018-02-08 17:30:23.000000',"fieldConst":4}]}}
        dict3 = {'header-field':{'a1':1},'item':{'image':{"field":3},'video1':[ {'image-datetime': '2018-02-08 17:30:23.000000',"fieldConst":4},
                                                                                {'image-datetime': '2018-02-08 17:30:24.000000',"fieldDiff":40}]}}
        miqtc.recursivelyUpdateDicts(dict1,dict2)
        self.assertTrue(dict1 == dict3)
        
        # twice same timestamp in update
        dict1 = {'header-field':{'a1':1},'item':{'image':{"field":3},'video1':{}}}
        dict2 = {'header-field':{'a1':1},'item':{'image':{"field":3},'video1':[ {'image-datetime': '2018-02-08 17:30:23.000000',"fieldConst":4},
                                                                                {'image-datetime': '2018-02-08 17:30:23.000000',"fieldDiff":40}]}}
        dict3 = {'header-field':{'a1':1},'item':{'image':{"field":3},'video1':[ {'image-datetime': '2018-02-08 17:30:23.000000',"fieldConst":4,"fieldDiff":40}]}}
        miqtc.recursivelyUpdateDicts(dict1,dict2)
        self.assertTrue(dict1 == dict3)
        
        # real example
        dict1 = {'with-UUID-MSM96_003_AUV-01_GMR_CAM-1_20180208_173022.jpg': [{  'image-camera-yaw-degrees': -175.0, 
                                                                                'image-datetime': '2018-02-08 17:30:22.000000',}],
                 'with-UUID-MSM96_003_AUV-01_GMR_CAM-2_20180208_173022.avi': [{  'image-datetime': '2018-02-08 17:30:22.000000', 
                                                                                'image-depth': 502.0, }, 
                                                                             {  'image-datetime': '2018-02-08 17:30:23.000000', 
                                                                                'image-depth': 501.0,  } ]}
        dict2 = {'with-UUID-MSM96_003_AUV-01_GMR_CAM-1_20180208_173022.jpg': {   'image-camera-yaw-degrees': -75.0,},
                 'with-UUID-MSM96_003_AUV-01_GMR_CAM-2_20180208_173022.avi': {   'image-camera-yaw-degrees': '165.0',},}
        dict3 = {'with-UUID-MSM96_003_AUV-01_GMR_CAM-1_20180208_173022.jpg': [{  'image-camera-yaw-degrees': -75.0, 
                                                                                'image-datetime': '2018-02-08 17:30:22.000000',}],
                 'with-UUID-MSM96_003_AUV-01_GMR_CAM-2_20180208_173022.avi': [{  'image-datetime': '2018-02-08 17:30:22.000000', 
                                                                                'image-depth': 502.0, 
                                                                                'image-camera-yaw-degrees': '165.0', }, 
                                                                             {  'image-datetime': '2018-02-08 17:30:23.000000', 
                                                                                'image-depth': 501.0, }]}
        miqtc.recursivelyUpdateDicts(dict1,dict2)
        self.assertTrue(dict1 == dict3)

        # item deletion
        dict1 = {'header-field':{'a1':1},'item':{'image':{"field":3},'video1':[{'image-datetime': '2018-02-08 17:30:24.000000',"fieldDiff":40}]}}
        dict2 = {'header-field':{'a1':1},'item':{'image':{"field":3},'video1':""}}
        dict3 = {'header-field':{'a1':1},'item':{'image':{"field":3},'video1':""}}
        miqtc.recursivelyUpdateDicts(dict1,dict2)
        self.assertTrue(dict1 == dict3)


    def test_recursivelyRemoveEmptyFields(self):
        dict1 = {'a':{'a1':1,'a2':2},'b':{'b1':2},'c':''}
        dict2 = {'a':{'a1':1,'a2':2},'b':{'b1':2}}
        dict1 = miqtc.recursivelyRemoveEmptyFields(dict1)
        self.assertTrue(dict1 == dict2)

        dict1 = {'a':{'a1':1,'a2':''},'b':{'b1':2},'c':""}
        dict2 = {'a':{'a1':1},'b':{'b1':2}}
        dict1 = miqtc.recursivelyRemoveEmptyFields(dict1)
        #pprint(dict1)
        self.assertTrue(dict1 == dict2)

        dict1 = {'a':{'a1':1,'a2':''},'b':{'c':""}}
        dict2 = {'a':{'a1':1}}
        dict1 = miqtc.recursivelyRemoveEmptyFields(dict1)
        #pprint(dict1)
        self.assertTrue(dict1 == dict2)

        dict1 = {'a':[{'a1':1,'a2':''}],'b':{'c':""}}
        dict2 = {'a':[{'a1':1}]}
        dict1 = miqtc.recursivelyRemoveEmptyFields(dict1)
        #pprint(dict1)
        self.assertTrue(dict1 == dict2)

        # remove white spaces
        dict1 = {'a':[{'a1':1,'a2':'\na '}],'b':{'c':" b\t"},'c':'    '}
        dict2 = {'a':[{'a1':1,'a2':'a'}],'b':{'c':"b"}}
        dict1 = miqtc.recursivelyRemoveEmptyFields(dict1)
        #print(DeepDiff(dict1,dict2))
        self.assertTrue(dict1 == dict2)

    def test_findCommonDictElements(self):

        dict1 = {1:1}
        dict2 = {1:1}
        common = miqtc.findCommonDictElements(dict1,dict2)
        self.assertTrue({1:1} == common)

        dict1 = 1
        dict2 = 2
        common = miqtc.findCommonDictElements(dict1,dict2)
        self.assertTrue({} == common)

        dict1 = 1
        dict2 = 1
        common = miqtc.findCommonDictElements(dict1,dict2)
        self.assertTrue(1 == common)

        dict1 = {1:1,
                 2:2}
        dict2 = {1:1}
        common = miqtc.findCommonDictElements(dict1,dict2)
        self.assertTrue({1:1} == common)

        dict1 = {1:{1:2},
                 2:2,
                 3:3}
        dict2 = {1:{1:2,3:3},
                 2:2}
        common = miqtc.findCommonDictElements(dict1,dict2)
        #print(common)
        self.assertTrue({1: {1:2}, 2:2} == common)

    def test_recursiveMakeNoneDictFieldsEmptyStr(self):

        d1 = {'a':1,
              'b':'test',
              'c':[1,2],
              'd':{'da':3,
                   'db':{'dba':2}}}
        correct = { 'a':"",
                    'b':"",
                    'c':"",
                    'd':{'da':"",
                        'db':{'dba':""}}}
        miqtc.recursiveMakeNoneDictFieldsEmptyStr(d1)
        self.assertTrue(correct == d1)

    def test_jsonschmeaType2PythonType(sekf):
        assert miqtc.jsonschemaType2PythonType("string") == str
        assert miqtc.jsonschemaType2PythonType("number") == float
        assert miqtc.jsonschemaType2PythonType("integer") == int
        assert miqtc.jsonschemaType2PythonType("array") == list
        assert miqtc.jsonschemaType2PythonType("object") == dict


    def test_resolve_jsonschema(self):

        schema = {
                    "$id": "https://example.com/schemas/customer",
                    "$schema": "https://json-schema.org/draft/2020-12/schema",
                    "type": "object",
                    "properties": {
                        "first_name": { "$ref": "#/$defs/name" },
                        "last_name": { "$ref": "#/$defs/test" },
                    },
                    "required": ["first_name", "last_name"],

                    "$defs": {
                        "name": { "type": "string" },
                        "test": { "sub-test":{"$ref": "#/$defs/name"} }
                    }
                  }
        resolved_expected = {
                    "$id": "https://example.com/schemas/customer",
                    "$schema": "https://json-schema.org/draft/2020-12/schema",
                    "type": "object",
                    "properties": {
                        "first_name": { "type": "string" },
                        "last_name": { "sub-test": { "type": "string" } },
                    },
                    "required": ["first_name", "last_name"],

                    "$defs": {
                        "name": { "type": "string" },
                        "test": { "sub-test": { "type": "string" } }
                    }
                  }
        resolved = miqtc.resolveJsonschema(schema,schema)
        assert resolved == resolved_expected

       
        resolved = miqtc.resolveJsonschema(miqtv.ifdo_schema,
                                            miqtv.ifdo_schema,
                                            [miqtv.annotation_schema, miqtv.provenance_schema])

        json_data = json.dumps(resolved, indent = 4, sort_keys=True)

        # write the JSON string to a file
        # path = "/home/kheger/Documents/delete_me/test.json"
        # with open(path, 'w') as f:
        #    f.write(json_data)
