from setuptools import setup

#version = {}
#with open("mariqt/version.py") as fp:
#    exec(fp.read(), version)

setup(
   name='mariqt',
   #version = version['__version__'],
   version='1.0.0',
   description='The MareHub & Marine Imaging Community Image QA/QC and curation toolbox',
   author='Timm Schoening',
   author_email='tschoening@geomar.de',
   long_description="We strive to make marine image data FAIR. We develop FDOs for images to establish a common language for marine imagery, we develop best-practice operating procedures for handling marine images and we develop the MarIQT software to apply the iFDOs and procedures to marine imagery. MarIQT stands for Image Quality control / quality assurance and curation Tools (IQT) conceptualised and developed by the MareHub working group on Videos/Images (part of the DataHub, a research data manangement initiative by the Helmholtz association).",
   packages=['mariqt','mariqt.sources'],
   install_requires=['numpy','pymap3d','utm','pyyaml','requests','deepdiff','python-xmp-toolkit','jsonschema[format]','packaging'],
   classifiers=[
    'Programming Language :: Python :: 3',
    'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
    'Operating System :: OS Independent'],
   include_package_data=True
)
